<?php
namespace App\Cpt;


/**
 * @param $name
 * @param array $args
 * https://developer.wordpress.org/resource/dashicons/#schedule
 */
function add_post_type($name, $args = array()){
    add_action('init', function() use($name, $args){
        $upper = ucwords($name);
        $name = strtolower(str_replace(' ', '_', $name));
        $args = array_merge(
            [
                'menu_icon' => 'dashicons-admin-home',
                'public'	=> true,
                'label'		=> "$upper",
                'labels'	=> ['add_new_item'=>"Add new $upper"],
                'supports' 	=> ['title','editor','thumbnail','page-attributes'],
                'exclude_from_search' => false,
                'capability_type'    => 'page'
            ],
            $args);
        register_post_type($name, $args);
    });
}
function add_taxonomy($name, $post_type, $args = array()){
    $name = strtolower($name);
    add_action('init', function() use($name, $post_type, $args){
        $args = array_merge(
            [
                'label' => ucwords($name),
                'show_ui' => true,
                'query_var' => true
            ],
            $args);
        //echo '<pre>';	var_dump($args);	echo '</pre>';
        register_taxonomy($name, $post_type,  $args);
    });
}


/**
 * update the permalinks after the add_post_type is created/modified
 */
add_post_type ( 'fleet', array (
    'name' => 'offer',
    'menu_icon' => 'dashicons-format-aside',
    'supports' 	=> ['title'],
    'label' => "Fleet",
    'supports' 	=> ['title','thumbnail'],
    'menu_icon'   => 'dashicons-format-aside',
    'show_admin_column' => true
    //'has_archive' => 'presse'
));

/*
 *  Team
 */

add_post_type ( 'team', array (
    'name' => 'offer',
    'menu_icon' => 'dashicons-format-aside',
    'supports' 	=> ['title'],
    'label' => "Team",
    'supports' 	=> ['title','thumbnail'],
    'menu_icon'   => 'dashicons-format-aside',
    'show_admin_column' => true
    //'has_archive' => 'presse'
));


/*add_taxonomy('boat','fleet',[
        'hierarchical' => true,
        'show_admin_column' => true
    ]
);*/

add_taxonomy('size','fleet',[
        'hierarchical' => true,
    'show_admin_column' => true
    ]
);

add_taxonomy('type','fleet',[
        'hierarchical' => true,
        'show_admin_column' => true
    ]
);

add_taxonomy('department','team',[
        'hierarchical' => true,
        'show_admin_column' => true
    ]
);