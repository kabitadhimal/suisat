;
acf.add_action('append', function( $el ){
    // $el will be equivalent to the new element being appended $('tr.row')
    // find a specific field
    //var $field = $el.find('#my-wrapper-id');
    adjustLayout($el);
});
acf.add_action('load', function( $el ){
    // $el will be equivalent to $('body')
    adjustLayout($el);
});

function adjustLayout($el){
    //.acf-option-customized has data-name='type'
    //acf-th acf-th-column_1    =>
    $ = jQuery;
    $('.acf-flexible-content .values .layout[data-layout="columns"] .acf-table').each(function(){
        var $self = $(this);
        if($self.attr('data-injected')) return;
        $self.attr('data-injected', '1');
        //create new row for the options
        var columnCount = $(this).find('tr:first th').length;
        var $optionRow = jQuery('<tr />').prependTo($(this));
        var $optionsTd = jQuery('<td />').attr('colspan',columnCount).appendTo($optionRow);

        var $optionsWrapper = $('<div />').addClass('acf-option-customized-wrapper').prependTo($optionsTd);
        $(this).find('.acf-option-customized').each(function(index){

            var $dropMenu = $(this).find('select, input[type=checkbox]');
            $(this).hide();

            //hide options
            var $dropMenuHeader = $self.find('th[data-key='+$(this).data("key")+']').hide();

            //add label to the option and append to the wrapper
            $('<label />').text($dropMenuHeader.text()).appendTo($optionsWrapper);

            var $dropMenuClone = $dropMenu.clone(true).appendTo($optionsWrapper);
            $dropMenuClone.change(function(e){
                if(e.currentTarget.getAttribute('type') == 'checkbox'){
                    //console.log(e.currentTarget.checked);
                    $dropMenu.prop( "checked", e.currentTarget.checked ).change();
                }else{
                    var val = $(this).find(":selected").val();
                    $dropMenu.val(val).change();
                }
            })
        });
    });

}