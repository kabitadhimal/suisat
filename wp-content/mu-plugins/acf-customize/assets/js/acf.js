(function($){
    acf.add_action('append', function( $el ){
        adjustColumnLayout($el);
        adjustHeroColumnLayout($el);
        adjustAccordionLayout($el);
    });

    acf.add_action('load', function( $el ){
        adjustColumnLayout($el);
        adjustHeroColumnLayout($el);
        adjustAccordionLayout($el);
    });

    function adjustColumnLayout($el){
        $('.acf-flexible-content .values .layout[data-layout="columns_layout"] .acf-fields [data-name="space_size"]').each(function(index){
            var $parent = $(this).parent();
            //original parent wrapper has css class acf-fields
            if($parent.attr('data-injected') == 1) return;
            $parent.attr('data-injected', 1);

            var $clearDiv = $('<div />').insertAfter($(this));
            $clearDiv.css({
                clear:'both', height:'2px'
            });

            $parent.find('[data-name="background"], [data-name="alignment"], [data-name="space_size"]').css({
                float:'left',
                clear:'none',
                "margin-right": '10px'
            });
        });

        /*
        $('.acf-flexible-content .values .layout[data-layout="columns_layout"] .acf-fields [data-name="background"]').each(function(index){
            var $parent = $(this).parent();
            //original parent wrapper has css class acf-fields
            if($parent.hasClass('acf-field')) return;

            var $wrapperDiv = $('<div />').prependTo($parent);
            $wrapperDiv.addClass('acf-field');

            $parent.find('[data-name="background"]').appendTo($wrapperDiv).removeClass('acf-field').css({
                float:'left',
                "margin-right": '10px'
            });
            $parent.find('[data-name="title"]').appendTo($wrapperDiv).removeClass('acf-field');
        });
        */
    }

    function adjustHeroColumnLayout($el){
        $('.acf-flexible-content .values .layout[data-layout="hero_column"] .acf-fields [data-name="space_size"]').each(function(index){
            var $parent = $(this).parent();
            //original parent wrapper has css class acf-fields
            if($parent.attr('data-injected') == 1) return;
            $parent.attr('data-injected', 1);

            var $clearDiv = $('<div />').insertAfter($(this));
            $clearDiv.css({
                clear:'both', height:'2px'
            });

            $parent.find('[data-name="background"], [data-name="type"], [data-name="space_size"]').css({
                float:'left',
                clear:'none',
                "margin-right": '10px'
            });
        });
    }

    function adjustAccordionLayout($el){
        $('.acf-flexible-content .values .layout[data-layout="accordion"] .acf-fields [data-name="type"]').each(function(index){
            var $parent = $(this).parent();
            //original parent wrapper has css class acf-fields
            if($parent.attr('data-injected') == 1) return;
            $parent.attr('data-injected', 1);

            var $clearDiv = $('<div />').insertAfter($(this));
            $clearDiv.css({
                clear:'both', height:'2px'
            });

            $parent.find('[data-name="background"], [data-name="type"], [data-name="space_size"]').css({
                float:'left',
                clear:'none',
                "margin-right": '10px'
            });
        });
        /*
        $('.acf-flexible-content .values .layout[data-layout="accordion"] .acf-fields [data-name="background"]').each(function(index){
            var $parent = $(this).parent();
            //original parent wrapper has css class acf-fields
            if($parent.hasClass('acf-field')) return;

            var $wrapperDiv = $('<div />').prependTo($parent);
            $wrapperDiv.addClass('acf-field').css('overflow','hidden');

            $parent.find('[data-name="background"], [data-name="type"]').appendTo($wrapperDiv).removeClass('acf-field').css({
                float:'left',
                "margin-right": '10px'
            });
            //$parent.find('[data-name="type"]').appendTo($wrapperDiv).removeClass('acf-field');
        });
        */

        $('.acf-flexible-content .values .layout[data-layout="accordion"] .acf-fields .acf-fields [data-name="expand_accordion"]').each(function(index){
            var $parent = $(this).parent();
            //original parent wrapper has css class acf-fields
            if($parent.attr('data-injected') == 1) return;
            $parent.attr('data-injected', 1);

            var $clearDiv = $('<div />').insertAfter($(this));
            $clearDiv.css({
                clear:'both', height:'2px'
            });

            $parent.find('[data-name="title"], [data-name="contains_table"], [data-name="expand_accordion"]').css({
                float:'left',
                clear:'none',
                "margin-right": '10px'
            });
            $parent.find('[data-name="title"]').css({
                width:'74%',
                "margin-right": 0
            });
        });


        //banner
        $('.acf-flexible-content .values .layout[data-layout="banner"] .acf-fields [data-name="alignment"]').each(function(index){
            var $parent = $(this).parent();
            //original parent wrapper has css class acf-fields
            if($parent.attr('data-injected') == 1) return;
            $parent.attr('data-injected', 1);

            var $clearDiv = $('<div />').insertAfter($(this));
            $clearDiv.css({
                clear:'both', height:'2px'
            });

            $parent.find('[data-name="background"], [data-name="type"], [data-name="alignment"]').css({
                float:'left',
                clear:'none',
                "margin-right": '10px'
            });
        });

        /*
        $('.acf-flexible-content .values .layout[data-layout="banner"] .acf-fields [data-name="title"]').each(function(index){
            var $parent = $(this).parent();
            //original parent wrapper has css class acf-fields
            if($parent.attr('data-injected') == 1) return;
            $parent.attr('data-injected', 1);

            var $clearDiv = $('<div />').insertAfter($(this));
            $clearDiv.css({
                clear:'both', height:'2px'
            });

            $parent.find('[data-name="type"], [data-name="title"], [data-name="alignment"]').css({
                float:'left',
                clear:'none',
                "margin-right": '10px'
            });
        });
        */

        adjustBackgroundSize();
    }

    /**
     * group space_size & background in a row
     * the layout must follow: background + space_size ( with space_size in the end )
     */
    function adjustBackgroundSize(){
        var modules = ['social_posts','testimonial', 'video', 'table','documents','contacts'];
        for(var x=0; x<modules.length;x++){
            //social //testimonial
            $('.acf-flexible-content .values .layout[data-layout='+modules[x]+'] .acf-fields [data-name="space_size"]').each(function(index){
                var $parent = $(this).parent();
                //original parent wrapper has css class acf-fields
                if($parent.attr('data-injected') == 1) return;
                $parent.attr('data-injected', 1);

                var $clearDiv = $('<div />').insertAfter($(this));
                $clearDiv.css({
                    clear:'both', height:'2px'
                });

                $parent.find('[data-name="background"], [data-name="space_size"]').css({
                    float:'left',
                    clear:'none',
                    "margin-right": '10px'
                });
            });
        }

    }

})(jQuery);
