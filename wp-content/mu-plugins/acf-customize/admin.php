<?php

/**
 * hide ACF dashboard on admin
 */
#require WPMU_PLUGIN_DIR.'/advanced-custom-fields-pro/cpt.php';

/**
 * https://www.advancedcustomfields.com/blog/acf-pro-5-5-13-update/
 * Faster load times!
 * disabe loading of scf custom tags
 */
add_filter('acf/settings/remove_wp_meta_box', '__return_true');

add_action('admin_enqueue_scripts', function($hook){
    global $typenow;
    if($typenow == 'page'){
        if($hook == 'post.php' || $hook == 'post-new.php'){
            wp_enqueue_script('kempinski-acf-customize', plugin_dir_url( __FILE__ ).'assets/js/acf.js', [], false, true);
            wp_enqueue_style('kempinski-acf-customize-css', plugin_dir_url( __FILE__ ).'assets/css/acf.css', [], false);
        }
    }
});

