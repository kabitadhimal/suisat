<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $pagenow, $wpdb, $wp;

$search_ids = array();
$term = $_GET['s'];
// search for variations with a matching sku and return the parent.
$sku_to_parent_id = $wpdb->get_col($wpdb->prepare("SELECT p.post_parent as post_id FROM {$wpdb->posts} as p join {$wpdb->postmeta} pm on p.ID = pm.post_id and pm.meta_key='_sku' and pm.meta_value LIKE '%%%s%%' where p.post_parent <> 0 group by p.post_parent", wc_clean($term)));
//Search for a regular product that matches the sku.
$sku_to_id = $wpdb->get_col($wpdb->prepare("SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key='_sku' AND meta_value LIKE '%%%s%%';", wc_clean($term)));
$search_ids = array_merge($search_ids, $sku_to_id, $sku_to_parent_id);

if(!empty($search_ids)) {

    $args = array(
        'post_type' => 'product',
        'post__in' => $search_ids,
        'suppress_filters' => false,
        'post_status' => 'publish',
    );

}else {

    $args = array(
        'post_type' => 'product',
        's' => $_GET['s'],
        'suppress_filters' => false,
        'post_status' => 'publish',
    );
}


$query = new WP_Query( $args );
$totalProducts = $query->found_posts;
?>
<div id="page-product-content" class="page-product__content">
    <div class="container container--sm">
    <div class="row page-product__grid">

        <div class="col-xs-12">
            <div class="page-product__col-inner">
                <!-- Product listing page banner -->
                <section class="block text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="70">
                    <header class="block__header m-b-2">
                        <div class="h1"><?php _e("Search Result for","app"); ?> : <?php printf( __( '%s', 'app' ), get_search_query() ); ?></div>
                    </header>
                    <!--<figure class="promo__pic hidden-xs">
                        <img alt="" src="<?php /*echo get_template_directory_uri(); */?>/assets/content/product/product-banner.jpg" />
                    </figure>-->
                </section><!-- Product listing page banner ends -->
                <!-- Product items block -->
                <div class="product-item-block wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.35s" data-wow-offset="70">
                    <?php if ( $query->have_posts() ) :
                        /**
                         * woocommerce_before_shop_loop hook.
                         *
                         * @hooked wc_print_notices - 10
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
                        // do_action( 'woocommerce_before_shop_loop' );
                        ?>
                        <div class="clearfix js-row flex flex--wrap product-item-block-row">

                            <?php //woocommerce_product_loop_start(); ?>

                            <?php // woocommerce_product_subcategories();

                            $count=1
                            ?>

                            <?php
                           // global $wp_query;
                            while ( $query->have_posts() ) : $query->the_post(); ?>

                                <?php
                                /**
                                 * woocommerce_shop_loop hook.
                                 *
                                 * @hooked WC_Structured_Data::generate_product_data() - 10
                                 */
                                //   do_action( 'woocommerce_shop_loop' );
                                ?>

                                <div class="col-xs-12 product-item js-last-col">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="product-item__content">
                                            <?php
                                            global  $product;
                                            $productImage = $product->get_image('shop_catalog' );
                                            if(!empty($productImage)):
                                                ?>
                                                <figure class="product-item__pic">
                                                    <!-- /* <img alt="" src="<?php /*/*echo get_template_directory_uri(); */?>/assets/content/product/product-item-img01.jpg" />*/-->
                                                    <?php  echo $product->get_image('shop_catalog' ); ?>
                                                </figure>
                                            <?php endif; ?>
                                            <div class="product-item__meta">
                                                <div class="product-item__title"><?php the_title(); ?></div>
                                                <?php
                                                echo
                                                    //$price = $product->get_price();
                                                $price = $product->get_price_html();
                                                $reference= $product->get_sku();
                                                if(!empty($price)):
                                                    ?>
                                                    <div class="product-item__price"><?php //$pice.get_woocommerce_currency_symbol()?> <?php $price ?></div>
                                                <?php endif; ?>
                                                <?php if(!empty($reference)) : ?>
                                                    <div class="product-details__ref-code"> <?php echo $reference; ?></div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                                $count++;
                            endwhile; // end of the loop. ?>
                        </div>
                        <?php
                        /**
                         * woocommerce_after_shop_loop hook.
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action( 'woocommerce_after_shop_loop' );
                        ?>

                    <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

                        <?php
                        /**
                         * woocommerce_no_products_found hook.
                         *
                         * @hooked wc_no_products_found - 10
                         */
                        do_action( 'woocommerce_no_products_found' );
                        ?>

                    <?php endif; ?>


                    <!-- Pagination to display more products -->
                </div><!-- /.Product items block ends -->

            </div>
        </div><!-- /.Listing page column list ends -->

    </div><!-- /.Listing page content grid ends -->
    </div>
</div><!-- /.Listing page content ends -->

