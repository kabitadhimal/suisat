<?php
/*
Template Name: Ship Listing Page
*/

$actionUrl = get_permalink();
//build query
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = [ 'post_type' => 'fleet', 'paged' => $paged, 'paged' => $paged,'order' =>'ASC', 'orderby'=>'title','posts_per_page' => -1,'post_status'=> 'publish'];
$taxQuery = [];
$searchQuery = '';
$inputShipName = '';
if(isset($_GET['boat'])) $inputShipName = $_GET['boat'];
if($inputShipName){
    $args['s'] = $inputShipName;
}

$inputSizeCategories = '';
if(isset($_GET['size'])) $inputSizeCategories = (int) $_GET['size'];
if($inputSizeCategories){
    $taxQuery = [
        'taxonomy' => 'size',
        'field'    => 'term_id',
        'terms'    => $inputSizeCategories,
    ];

    $args['tax_query'] = [$taxQuery];
}
$query = new WP_Query( $args );
?>
<section class="section">
    <div class="section__bar">
        <div class="container container--no-padding">
            <h2 class="text-left">Fleet</h2>
            <div class="flex-container">

                    <div class="form-filter">
                        <div class="form-row">
                            <div class="form__col--size1">
                                <div class="form-group">
                                    <label for="select-1" class="label-block-frm-vassel">Select a <span>Vessel</span></label>
                                    <select id="select-1" class="form-control js-block-frm-vessel" name="vessel">
                                        <option value="">Select a Vessel</option>
                                        <?php
                                        $args = array('post_type'=>'fleet',
                                            'order' =>'ASC',
                                            'ignore_custom_sort' => TRUE,
                                            'orderby'=>'title',
                                            'posts_per_page' => -1,
                                            'post_status'=>'publish');
                                        $fleetPost = get_posts( $args );
                                        foreach($fleetPost as $post ) : setup_postdata( $post );
                                            $title = get_the_title();
                                            ?>
                                            <option value="<?=get_the_permalink()?>"  ><?=$title?></option>
                                            <?php
                                        endforeach;
                                        wp_reset_postdata();?>

                                    </select>
                                </div><!-- /.form-group -->
                            </div>
                            <form method="get" id="block-frm">
                            <div class="form__col--size2">
                                <div class="form-group">
                                    <label for="select-2">Sort by</label>
                                    <select id="select-2" class="select form-control js-block-frm-input-size" name="size">
                                        <option value="">All Sizes</option>
                                        <?php
                                        $sizeTerms = get_terms( 'size', array(
                                            'hide_empty' => false,
                                        ) );
                                        if(!empty($sizeTerms)):
                                            foreach($sizeTerms as $category):
                                                if(!empty($inputSizeCategories) && ($category->term_id==$inputSizeCategories)) :
                                                    $selected = 'selected';
                                                else:
                                                    $selected ='';
                                                endif;
                                                ?>
                                                <option value="<?=$category->term_id?>" <?=$selected?> ><?=$category->name?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </div><!-- /.form-group -->
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- /.container -->
        </div><!-- /.container -->
    </div><!-- /.section__bar -->
    <div class="section__body">
        <div class="container container--no-padding">
            <?php $fleetList = get_field('link');
            if(!empty($fleetList)):
                if(!empty($fleetList['target'])) :
                    $target='target="_blank"';
                endif;
                ?>
                <div class="text-left" style="padding-top: 30px;">
                    <a class="btn btn--yellow" href="<?=$fleetList['url']?>" <?=$target?>><?=$fleetList['title']?></a>
                </div>
            <?php endif; ?>

            <script>
                jQuery(document).ready(function () {
                    var $blockForm = jQuery('#block-frm');
                    jQuery('#block-frm .js-block-frm-input-size').change(function () {
                        $blockForm.submit();
                    });
                    jQuery('.js-block-frm-vessel').on('change', function(){
                        window.location = jQuery(this).val();
                    });
                });
            </script>

            <ul class="list-images" style="padding-top: 30px;">
                <?php
                if($query->have_posts()):
                    while($query->have_posts()): $query->the_post();
                        //get_template_part('partials/news-item');ship-list
                        ?>
                        <li>
                            <?php
                            // news-thumb
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
                            if($image) { $img = \App\getImageManager()->resize( \App\getImageDirectoryPath($image[0]), \App\IMAGE_SIZE_BLOCK_THUMB); }
                            ?>
                            <a  href="<?php the_permalink(); ?>" style="background-image: url(<?php echo $img; ?>)">
                                <img src="<?php echo $image[0]; ?>">
                                <?php the_title('<h2>','</h2>'); ?>
                            </a>
                        </li>
                        <?php
                    endwhile;
                else :
                    echo 'There is no any ships related to the selected criteria.';
                endif;

                wp_reset_postdata();
                ?>

        </div><!-- /.container -->
    </div><!-- /.section__body -->
</section><!-- /.section -->
<script>
    jQuery(function($) {
        var paramSize = jQuery('#select-2 option:selected').val();
        jQuery('#select-2 ~ .select-styled').text(jQuery('li[rel="'+ paramSize +'"]').text());
    });
</script>
