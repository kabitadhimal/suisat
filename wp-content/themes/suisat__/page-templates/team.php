<?php
/*
Template Name: Team Page
*/

//build query
$searchQuery = '';
$inputDepartCategories = '';
if(isset($_GET['department'])) $inputDepartCategories = (int) $_GET['department'];
$departTerms = get_terms( 'department', array(
    'hide_empty' => false,
    'orderby' => 'name'
) );
?>
<section class="section">
    <div class="section__bar">
        <div class="container container--no-padding">
            <h2>Team</h2>
            <form method="get" class="form-filter"  id="block-frm" >
                <div class="form-row">
                    <div class="form__col--size1">
                        <div class="form-group">
                            <label for="select-1">Sort by</label>

                            <select id="select-1" class="select form-control js-block-frm-input" name="department">

                                <option value="">All Departments</option>

                                <?php

                                if(!empty($departTerms)):
                                    foreach($departTerms as $category):
                                        ?>
                                        <option value="<?=$category->term_id?>"><?=$category->name?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div><!-- /.form-group -->
                    </div>

                </div>
            </form>
        </div><!-- /.container -->
    </div><!-- /.section__bar -->

    <div class="section__body">
        <div class="container container--no-padding js-filter-contents">
        </div><!-- /.container -->
    </div><!-- /.section__body -->
</section><!-- /.section -->

<script>

    //For Desktop
    jQuery( document ).ready(function() {
        var $blockForm = jQuery('#block-frm');
        jQuery('#select-1 ~ ul.select-options li').click(function() {
            var $cat = jQuery(this).attr('rel');
            submitForm($cat)
        });

        //For mobile
       jQuery('form select').on('change', function() {
            var val = jQuery(this).val();
            submitForm(val)
        });
        submitForm();
    });

    function submitForm($cat) {
        // var frmData = '<?php echo $jaxTag ?>type=<?=$contentType?>'+'&cat='+$catFilter +'&action=display_contents';
        var data = 'cat='+$cat +'&action=team_members';
        var ajaxURL = "<?=admin_url('admin-ajax.php')?>";
        $.ajax({
            url: ajaxURL,
            data: data,
            success: function(response){
                if (response.length > 0) {
                    //    $(".not-found").hide();
                    $(".js-filter-contents").html(response);
                    // == Renit javascript codes related to these blocks
                }/*else {
                 $(".not-found").show();
                 }*/
            }
        });
    }
</script>