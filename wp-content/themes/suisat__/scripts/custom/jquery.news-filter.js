// Building isotope layout for news listing posts
;(function() {
	// Cache isotope wrapper element
	var $newsListingElm = $('#block-media-gird');
	console.log("hi");

	// Build isotope layout
	var $newsListingRow = $newsListingElm.isotope({
		isInitLayout: true,
		itemSelector: '.col-sm-4',
		layoutMode: 'moduloColumns'
	});


	$('.grid').isotope({
	  itemSelector: '.grid-item',
	  percentPosition: true,
	  masonry: {
	    columnWidth: '.grid-sizer'
	  }
	});

})(jQuery);