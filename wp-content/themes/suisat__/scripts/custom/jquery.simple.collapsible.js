/**
 * Simple js collapsible UI
 */

/* ============================================================= */
/*     Initializing Function/Methods before page/window load     */
/* ============================================================= */

// https://stackoverflow.com/questions/12661797/jquery-click-anywhere-in-the-page-except-on-1-div

;(function($) {
    // Caching DOM elements into varaibles
    var $doc = $(document),
        $target = $('.js-collapsible');

    // Initialize simpleCollapsibleUI
	simpleCollapsibleUI($($target));

	// Function to create simple collapsible ui
    function simpleCollapsibleUI($item) {
        if($item.length === 0) return;    
        
        $item.each(function() {
            var $self = $(this);
            
            // Hide all targets on load
            $self.next($('.' + $('.js-collapsible').attr('data-toggle-target'))).hide();

            //Attch click event
            $self.on('click', function(e) {
                var $target = $self.next($('.' + $self.attr('data-toggle-target'))),
                    $selfParent = $self.parent(),
                    $collapsibleInMenu = $('.menu-category__list .js-collapsible');
                $('.menu-category__list li').removeClass('active-parent');

                if($('.js-collapsible').hasClass('expanded') && $('.js-collapsible').next($('.' + $('.js-collapsible').attr('data-toggle-target'))).attr('style') !== 'block') { // && $('.js-collapsible').next().attr('style') === 'block'
                    $('.js-collapsible').not($self).removeClass('expanded');
                    $('.menu-category__list li').not($selfParent).removeClass('active active-parent');
                    $('.js-collapsible').next($('.' + $('.js-collapsible').attr('data-toggle-target'))).not($target).slideUp({
                        duration: 300,
                        easing: 'easeInOutExpo'
                    });
                }

                $self.toggleClass('expanded');
                $selfParent.toggleClass('active');
                $target.stop(true).slideToggle({
                    duration: 450,
                    easing: 'easeInOutExpo'
                });
                e.preventDefault();
                e.stopPropagation();

                // $target.on('click', function(evt) {
                //     evt.stopPropagation();
                // });
            });
        });        
    }

    // Click on document to close all opened UIs
    // $doc.on('click', function(event) {
    //     if(event.target == $target || event.target.children() == $target) return;
    //     //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
    //     if($(event.target).closest($target).length) return;

    //     //Do processing of click event here for every element except with target
    //     if($target.hasClass('expanded')) {
    //         $target.filter('.expanded').trigger('click');
    //     } 
    // });
})(jQuery);