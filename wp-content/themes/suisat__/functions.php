<?php

use App\Psr4AutoloaderClass;

if(!class_exists('App\Psr4AutoloaderClass')) {
    require __DIR__ . '/src/autoloader.php';
    Psr4AutoloaderClass::getInstance()->addNamespace('Procab', __DIR__.'/src/Procab');
    Psr4AutoloaderClass::getInstance()->addNamespace('App', __DIR__.'/src/App');
}
$include_files = [
    'src/helpers.php',
    'src/setup.php',
    'src/filters.php',
   // 'src/Woo/filters.php',
    'src/Procab/Wp/Wrapper.php'
];

array_walk($include_files, function($file){
    if(!locate_template($file, true, true)){
        trigger_error(sprintf('Couldnot find %s', $file), E_USER_ERROR);
    }
});

unset($include_files);
/*
 * Social Feeds
 */

function getInstagramFeeds($limit = 1){
    //$instaUrl = 'https://www.instagram.com/edoxswisswatches/';
    $instaUrl = get_field('instagram_url','options');

    $curl_handle=curl_init();
    curl_setopt($curl_handle,CURLOPT_URL, $instaUrl.'?__a=1');
    curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
    curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
    $buffer = curl_exec($curl_handle);
    curl_close($curl_handle);

    if (empty($buffer)) return [];
    $items = [];

    $counter = 1;
    $instagramItems = json_decode($buffer);

    foreach($instagramItems->graphql->user->edge_owner_to_timeline_media->edges as $edge){
        $instagramItem = $edge->node;
        //var_dump($node->thumbnail_src, $node->code);
        $items[] = array('https://www.instagram.com/p/'.$edge->node->shortcode.'/', $edge->node->thumbnail_src);
        if($limit == $counter) return $items;
        $counter++;
    }
    return $items;
}

/*
 *  Walker Menu
 */


class Walker_Quickstart_Menu extends Walker_Nav_Menu {
    // add classes to ul sub-menus

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        switch($depth){
            case 0:

                $output .= "";
                break;
            default:
                $output .= "<ul>\n";
                break;
        }
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );

        if($depth == 0){
            $output .= "{$n}";
        } elseif ($depth==1) {
            $output .= "{$n}";
        } elseif($depth==2) {
            $output .= " </ul></div>{$n}";
        }else{
            $output .= "{$depth}</ul>{$n}";
        }

    }

    private function getElementAttributes($item, $depth, $args){
        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';
        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }
        return $attributes;
    }

    private function getElementCssClass($item, $depth, $args){
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        //$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

     //   $class_names = str_replace('menu-item-has-children', 'has-submenu', esc_attr($class_names));

        return $class_names;
    }



    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        // $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );
        //$item_output = $args->before;
        //$item->title = $item->title . ' => '. $depth;
        $item_output = '';

        $class_names = $this->getElementCssClass($item, $depth, $args);
        $attributes = $this->getElementAttributes($item, $depth, $args);

        $class_names = str_replace("current-menu-item","active",$class_names);
        $class_names = str_replace("current-menu-parent","active",$class_names);


        switch($depth){
            case 0:

                if (strpos($class_names, 'menu-item-has-children') == false) {
                    $item_output .="<li class='{$class_names} nav-item'><a {$attributes} class='nav-link'>{$item->title}</a>";
                } else {
                    $item_output .= "  <li class='nav-item dropdown'><a class='{$class_names} nav-link dropdown-toggle' {$attributes}>{$item->title}</a><div class='dropdown-menu'>";

                }
                break;
            case 1:
              //  $item_output .= '<div class="col-sm-6 col-xs-12 menu-category__group"><div class="'.$class_names.' menu-category__group-inner ">';
                if (strpos($class_names, 'menu-item-has-children') == false) {
                    $item_output .="<a class='{$class_names} dropdown-item' {$attributes}>{$item->title}  </a>";
                } else {
                    $item_output .= "";
                }
                break;
            default:
                $item_output .="<li class='{$class_names} nav-item'><a {$attributes} class='nav-link'>{$item->title}</a>";
                break;
        }
        $output .= $item_output;


    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }


        $class_names = $this->getElementCssClass($item, $depth, $args);


        switch($depth){
            case 0:
                if (strpos($class_names, 'menu-item-has-children') == true) {
                    $output .= '</div></li>';
                }else {
                    $output .= "</li>";
                }
                break;
            case 1:
                $output .= "";
                break;
            default:
                $output .= "";
                break;
        }



    }


}

/**
 * Enables the Excerpt meta box in Page edit screen.
 */
function wpcodex_add_excerpt_support_for_pages() {
    add_post_type_support( 'post', 'excerpt' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_pages' );
/*
 *
 */

/*function create_posttype() {
    register_post_type( 'acme_product',
        array(
            'labels' => array(
                'name' => __( 'Products' ),
                'singular_name' => __( 'Product' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'products'),
        )
    );
}
add_action( 'init', 'create_posttype' );*/