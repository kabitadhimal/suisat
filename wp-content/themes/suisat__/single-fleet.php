<?php $photo = get_field('banner_image');
if($photo): $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_TOP_BANNER); endif; ?>
<div class="intro intro--interior" style="background-image: url(<?php echo $photo; ?>)">
    <!--<div class="intro__content">
        <?php /*the_title('<h2>','</h2>'); */?>
    </div>--><!-- /.intro__content -->
</div><!-- /.intro -->

<?php $box1 = get_field('box_1'); ?>
<section class="section ship-description">
    <div class="cols">

        <div class="col col-a">
            <a class="popup-trigger" href="" data-toggle="modal" data-target="#exampleModal">
                <h2>
                    <i class="ico-pin"></i>
                    Ship position
                    <i class="ico-arrow-right"></i>
                </h2>
            </a>
        </div>

        <?php if(!empty($box1)): ?>
            <div class="col col-b">
                <div class="content__inner">
                    <?php if(!empty($box1['title'])): ?>
                        <h2><?=$box1['title']?></h2>
                    <?php endif; ?>
                    <?php if(!empty($box1['text'])):
                        echo $box1['text'];
                    endif; ?>
                </div><!-- /.section__inner -->
            </div><!-- /.col col-/-size1 -->
        <?php endif; ?>

        <div class="col col-c" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/temp/section-1.jpg)">
                <ul class="list-links">
                    <?php
                    $box2 = get_field("box_2");
                    $tc = $box2['tc_description'];
                    if(!empty($tc)):
                        if(!empty($tc['target'])) :
                            $target='target="_blank"';
                        endif;
                    ?>
                        <li> <a href="<?=$tc['url']?>" <?=$target?>>T/C description</a></li>
                    <?php endif; ?>

                    <?php $generalArrang=$box2['general_arrangement'];
                    if(!empty($generalArrang)):
                    ?>
                    <li>
                        <?php
                        if(!empty($generalArrang['target'])) :
                            $target='target="_blank"';
                        endif;
                        ?>
                        <a href="<?=$generalArrang['url']?>" <?=$target?>>General arrangement plan</a>
                    </li>
                    <?php endif;
                    $capacityPlan = $box2['capacity'];
                    if(!empty($capacityPlan)):
                    ?>

                    <li>
                        <?php
                            if(!empty($capacityPlan['target'])) :
                                $target='target="_blank"';
                            endif;
                        ?>
                        <a href="<?=$capacityPlan['url']?>" <?=$target?>>Capacity plan</a>
                    </li>
                    <?php endif; ?>
                </ul><!-- /.list-links -->

        </div><!-- /.col col-/-size2 -->
    </div><!-- /.cols -->
</section>
<?php $vesselId = get_field('box_3'); ?>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <script type="text/javascript">
                width='100%';	// the width of the embedded map in pixels or percentage
                height='650';	// the height of the embedded map in pixels or percentage
                border='1';	// the width of the border around the map (zero means no border)
                shownames='false';	// to display ship names on the map (true or false)
                latitude='37.4460';	// the latitude of the center of the map, in decimal degrees
                longitude='24.9467';	// the longitude of the center of the map, in decimal degrees
                zoom='9';	// the zoom level of the map (values between 2 and 17)
                maptype='1';	// use 0 for Normal Map, 1 for Satellite, 2 for OpenStreetMap
                trackvessel='<?=$vesselId['vessel_id']?>';	// MMSI of a vessel (note: vessel will be displayed only if within range of the system) - overrides "zoom" option
                fleet='';	// the registered email address of a user-defined fleet (user's default fleet is used)
            </script>
            <script type="text/javascript" src="//www.marinetraffic.com/js/embed.js"></script>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<section class="section section--simple section--table">
    <div class="container">

        <?php $descTitle = get_field("desc_title");
            if(!empty($tc)):
        ?>
        <header class="section__head">
            <h2><?=$descTitle?></h2>
            <?php
            $downnloadPdf = get_field('download_pdf');
            if(!empty($downnloadPdf)):
            ?>
            <a href="<?=$downnloadPdf?>" class="link-more" <?=$target?>>Download PDF file</a>
            <?php endif; ?>
        </header><!-- /.section__head -->
        <?php endif;?>
        <?php $generalInfo = get_field("general_information");
            if(!empty($generalInfo)):
        ?>
        <table class="table table-striped">
            <?php if(!empty($generalInfo['vessels_name'])): ?>
                <thead>
                <tr>
                    <th colspan="2"><?=$generalInfo['vessels_name']?></th>
                </tr>
                </thead>
            <?php endif; ?>

            <tbody>

            <?php if(!empty($generalInfo['vessels_name'])): ?>
                <tr>
                    <td style="width: 38.65%">Vessel’s name</td>

                    <td class="td--blue"><?=$generalInfo['vessels_name']?></td>
                </tr>
            <?php endif;?>

            <?php if(!empty($generalInfo['verssel_pname_date'])): ?>
                <tr>
                    <td style="width: 38.65%">Vessel’s previous name(s) and date(s) <br>of change</td>
                    <td class="td--blue">N/A</td>
                </tr>
            <?php endif;?>

            <?php if(!empty($generalInfo['flag'])): ?>
            <tr>
                <td style="width: 38.65%">Flag</td>
                <td class="td--blue"><?=$generalInfo['flag']?></td>
            </tr>
            <?php endif;?>

            <?php if(!empty($generalInfo['monthyear_built'])): ?>
                <tr>
                    <td style="width: 38.65%">Month/Year and where built</td>
                    <td class="td--blue"><?=$generalInfo['monthyear_built']?></td>
                </tr>
            <?php endif;?>


            <?php if(!empty($generalInfo['monthyear_built'])): ?>
            <tr>
                <td style="width: 38.65%">Yard name and number</td>

                <td class="td--blue"><?=$generalInfo['yard_name_number']?></td>
            </tr>
            <?php endif;?>
            <?php if(!empty($generalInfo['official_registration_number'])): ?>
            <tr>
                <td style="width: 38.65%">Official registration number</td>

                <td class="td--blue"><?=$generalInfo['official_registration_number']?></td>
            </tr>

            <?php endif;?>
            <?php if(!empty($generalInfo['imoclass_number'])): ?>
            <tr>
                <td style="width: 38.65%">IMO/Class number</td>

                <td class="td--blue"><?=$generalInfo['imoclass_number']?></td>
            </tr>

            <?php endif;?>
            <?php if(!empty($generalInfo['other'])): ?>
            <tr>
                <td style="width: 38.65%">Other</td>

                <td class="td--blue"><?=$generalInfo['other']?></td>
            </tr>
            <?php endif;?>
            <?php if(!empty($generalInfo['part_of_registry'])): ?>
            <tr>
                <td style="width: 38.65%">Port of Registry</td>

                <td class="td--blue"><?=$generalInfo['part_of_registry']?></td>
            </tr>
            <?php endif;?>
            <?php if(!empty($generalInfo['operational_purposes_info'])): ?>
            <tr>
                <td style="width: 38.65%">Owner full style and contact numbers for operational purposes, if appropriate</td>

                <td class="td--blue"><?=$generalInfo['operational_purposes_info']?></td>
            </tr>
            <?php endif;?>

            <?php if(!empty($generalInfo['operational_contact_number'])): ?>
            <tr>
                <td style="width: 38.65%">Managers full style and contact numbers for operational purposes, if appropriate</td>

                <td class="td--blue"><?=$generalInfo['operational_contact_number']?></td>
            </tr>
            <?php endif;?>
            </tbody>
        </table>
        <?php endif; ?>
        <?php if(!empty($downnloadPdf)): ?>
        <div class="section__actions">
            <a href="<?=$downnloadPdf?>" class="btn btn--yellow">Download PDF file</a>
        </div><!-- /.section__actions -->
        <?php endif; ?>
    </div><!-- /.container -->
</section>

<section class="section">
    <div class="container container--no-padding">
        <ul class="list-images list-gallery">
            <?php $shipGallery = get_field("ship_gallery");
            if(!empty($shipGallery)):
            foreach ($shipGallery as $image):
                $title = $image['title'];
                $fullPhoto = $image['url'];
                if($fullPhoto): $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($fullPhoto), \App\IMAGE_SIZE_GALLERY); endif; ?>
                <li>
                    <a class="image-popup-vertical-fit" href="<?php echo $fullPhoto; ?>" style="background-image: url(<?php echo $photo; ?>)">
                        <img src="<?php echo $photo; ?>" alt="">
                        <?php if(!empty($title)): ?>
                            <!--<h2><?/*=$title*/?></h2>-->
                        <?php endif; ?>
                    </a>
                </li>
            <?php endforeach;
                endif;
            ?>
        </ul><!-- /.list-images -->
        <div class="text-center" style="padding-bottom: 80px;">
            <a class="btn btn--yellow" href="<?php echo home_url('/all-fleet/'); ?>">See all fleet </a>
        </div>
    </div><!-- /.container -->
</section>