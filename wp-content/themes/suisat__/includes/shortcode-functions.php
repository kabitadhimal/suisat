<?php
function kd_button($atts,$content) {
	extract(shortcode_atts(array(
		'type' =>'yellow',
		'href' =>'',
		'target' =>'',
	), $atts));

	$button='';

	if(!empty($target)):
		$tar = 'target="_blank"';
	endif;


	if($type=="yellow") {

		  $button = '<a class="btn btn--yellow" href="'.$href.'" ' .$tar. '>'.$content.'</a>';

	}else if($type=="grey") {
		 $button = '<a class="btn btn--grey" href="'.$href.'" ' .$tar. '>'.$content.'</a>';
	}

	return  $button;
}

function register_shortcodes(){
    //arrow
    add_shortcode('btn', 'kd_button');
}
add_action( 'init', 'register_shortcodes');
