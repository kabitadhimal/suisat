<?php

namespace App;

// //https://github.com/bradvin/social-share-urls
use Intervention\Image\ImageManager;
use Procab\Media\Manager;
use Procab\Wp\Wrapper;

function isRequestAjax()
{
    return Wrapper::$isRequestAjax;
}




function get_main_template(){
    return Wrapper::$mainTemplate;
}

const IMAGE_SIZE_THUMBNAIL = "thumbanil";
const IMAGE_SIZE_HOME_THUMBNAIL = "home-thumbanil";
const IMAGE_SIZE_HOME_MAIN = "home-main-image";
const IMAGE_SIZE_VISION_SLIDER = "home-vision-slider";
const IMAGE_SIZE_BLOCK_THUMB = "image-size-block-thumb";
const IMAGE_SIZE_TOP_BANNER = "image-size-top-banner";
const IMAGE_SIZE_GALLERY = "image-size-gallery";



function getGoogleMapKey(){
    static $key;
    if(!$key){
        $key = get_field('google_map_api_key', 'option');
    }
    return $key;
}


/**
 * @return Manager
 */
function getImageManager(){
    ini_set('memory_limit', '512M');
    static $imageManager;
    if(!$imageManager) {
        $manager = new ImageManager(['driver' => 'gd']);
        $config = [
            'sizes' => [
                IMAGE_SIZE_THUMBNAIL => [50,50,'fit'],
                IMAGE_SIZE_HOME_THUMBNAIL => [1920,870,'fit'],
                IMAGE_SIZE_VISION_SLIDER => [1560,788,'fit'],
                IMAGE_SIZE_BLOCK_THUMB => [395,402,'fit'],
                IMAGE_SIZE_TOP_BANNER => [1920,652,'fit'],
                IMAGE_SIZE_GALLERY => [400,400,'fit'],
            ],
            'save_folder' => ABSPATH.'/cache/images',
            'save_url' => site_url(). '/cache/images'
        ];
        $imageManager = new Manager($manager, $config);
    }
    return $imageManager;
}

/**
 * get the directory path of the image
 * @param $imageUri
 * @return string
 * todo check if the image belongs to current website
 */
function getImageDirectoryPath($imageUri){
    static $uploadDir;
    if(!$uploadDir) $uploadDir = wp_get_upload_dir();
    $imageUri = str_replace($uploadDir['baseurl'], '', $imageUri);
  return $uploadDir['basedir'].$imageUri;
}

function is_content_empty($str) {
    return trim(str_replace('&nbsp;','',strip_tags($str))) == '';
}
function is_post_content_empty(\WP_Post $post) {
    return is_content_empty($post->post_content);
}

/*
 *  Excerpt
 */
/*add_filter('hidden_meta_boxes', 'show_hidden_meta_boxes', 10, 2);
function show_hidden_meta_boxes($hidden, $screen) {
    if ( 'post' == $screen->base ) {
        foreach($hidden as $key=>$value) {
            if ('postexcerpt' == $value) {
                unset($hidden[$key]);
                break;
            }
        }
    }

    return $hidden;
}*/

/**
 * @param $_acf_field_value
 * @return mixed
 */
function _ps_gap_selection ($_acf_field_value){
    $section_gap_type = '';
    if ( !empty($_acf_field_value) && $_acf_field_value !== 'default' ) :
        switch ( $_acf_field_value ) :
            case "py":
                $section_gap_type = $_acf_field_value . '-0';
                break;

            case "pt":
                $section_gap_type = $_acf_field_value . '-0';
                break;
            case "pb":
                $section_gap_type = $_acf_field_value . '-0';
                break;
        endswitch;
    endif;
    return $section_gap_type;
}

/*
 * Get YoutubeVideo ID
 */

function getYoutubeVideoID($url) {
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
    return $match[1];
}

/* Get Start Time of  the youtube video
 *
 */
function getStartTimeYoutubeVideo($url) {

    $startTimeT = explode('?t=',$url);
    $startTimeStart = explode('?start=',$url);
    if(!empty($startTimeT[1])) :
        $start = '&amp;start='.$startTimeT[1];
    elseif(!empty($startTimeStart[1])) :
        $start = '&amp;start='.$startTimeStart[1];
    endif;

    return $start;
}

/*
 * pagination
 */

/**
 * @param string $numpages
 * @param string $pagerange
 * @param string $paged
 * http://callmenick.com/post/custom-wordpress-loop-with-pagination
 */
function custom_pagination($numpages = '', $pagerange = '', $paged='', $base = '') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     *
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if(!$numpages) {
            $numpages = 1;
        }
    }

    $base = ($base) ? : get_pagenum_link(1);

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function.
     */
    $pagination_args = array(
        //'base'            => get_pagenum_link(1) . '%_%',
        'base'            => $base.'%_%',
        'format'          => 'page/%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => False,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => True,
        'prev_text'       => __('&laquo;'),
        'next_text'       => __('&raquo;'),
        'type'            => 'plain',
        'add_args'        => false,
        'add_fragment'    => '',
    );

    $paginate_links = paginate_links($pagination_args);

    $paginate_links = str_replace('<a','<li><a', $paginate_links);
    $paginate_links = str_replace('/a>','/a></li>', $paginate_links);
    $paginate_links = str_replace("<span aria-current='page' class='page-numbers current'",'<li class="active"><span', $paginate_links);
    $paginate_links = str_replace("<span class='page-numbers current'",'<li class="active"><span', $paginate_links);
    $paginate_links = str_replace('<span class="page-numbers dots"','<li class="disabled"><span', $paginate_links);
    $paginate_links = str_replace('</span>','</span></li>', $paginate_links);

    if ($paginate_links) {
        echo "<nav class='custom-pagination'><ul class='pagination pagination--light'>";
        echo $paginate_links;
        echo "</ul></nav>";
    }
}