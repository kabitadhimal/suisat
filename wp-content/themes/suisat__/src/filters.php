<?php
/**
 * Created by PhpStorm.
 * User: laxman
 * Date: 7/8/2017
 * Time: 9:47 PM
 */

/**
 * add wrapper - template inheritance in wp
 */
add_filter('template_include', ['Procab\\Wp\\Wrapper', 'wrap'], 200);


function procab_wp_title( $title, $sep ) {
    global $paged, $page;
    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', 'RJ' ), max( $paged, $page ) );
    }
    return $title;
}
add_filter( 'wp_title', 'procab_wp_title', 10, 2 );

//remove stress characters, stress characters

add_filter('wp_handle_upload_prefilter', 'procab_upload_filter',1,1 );

function procab_upload_filter( $file ){
        $file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/','-', $file['name']);
    return $file;
}
/*
 * Redirect to home page if author is 1
 */

add_action('template_redirect', 'procab_template_redirect');
function procab_template_redirect(){
    if (is_author()){
        wp_redirect( home_url() ); exit;
    }
}


remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 12);

/*
* Callback function to filter the MCE settings
*/
// Callback function to filter the MCE settings
function app_mce_before_init_insert_formats( $init_array ) {
    // Define the style_formats array
    $style_formats = [
        [
            'title' => 'Yellow Class',
            'selector' => 'a',
            'classes' => 'btn btn--yellow'
        ],
        [
            'title' => 'Small Button Class',
            'selector' => 'a',
            'classes' => 'btn btn--grey '
        ]
    ];
    $init_array['style_formats'] = json_encode( $style_formats );
    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'app_mce_before_init_insert_formats' );

/*

add_action( 'wp_ajax_fleet_contents', 'fleet_contents_callbck' );
add_action( 'wp_ajax_nopriv_fleet_contents', 'fleet_contents_callbck' );

function fleet_contents_callbck() {
        extract($_GET);
        include __DIR__.'/../partials/block/filter-items.php';
        die();
}


add_action( 'wp_ajax_display_contents', 'display_contents_callbck' );
add_action( 'wp_ajax_nopriv_display_contents', 'display_contents_callbck' );
function display_contents_callbck() {
    extract($_GET);
    include __DIR__.'/../partials/block/size-filter.php';
    die();
}*/

/**/

/*add_action( 'wp_ajax_news_contents', 'news_contents_callbck' );
add_action( 'wp_ajax_nopriv_news_contents', 'news_contents_callbck' );
function news_contents_callbck() {
    extract($_GET);
    include __DIR__.'/../partials/block/news-filter.php';
    die();
}*/


add_action( 'wp_ajax_team_members', 'team_members_callbck' );
add_action( 'wp_ajax_nopriv_team_members', 'team_members_callbck' );
function team_members_callbck() {
    extract($_GET);
    include __DIR__.'/../partials/block/team-filter.php';
    die();
}
