<?php

/*
**
* Set the content width based on the theme's design and stylesheet.
 *
 * @since SEIC 1.0
 */
if (! function_exists('procab_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @sinceprocab 1.0
     */
    function procab_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based onprocab, use a find and replace
* to change 'procab' to the name of your theme in all the template files
*/
        load_theme_textdomain('app');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');
        add_theme_support( 'site-logo', array( 'size' => 'full' ) );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(158, 240, true);
        //add_image_size('block-thumb',395,402,false);

              // Registers a single custom Navigation Menu in the custom menu editor
        //  register_nav_menu('primary', __('Primary Menu', 'procab'));



            register_nav_menu( 'primary_menu', __( 'Main Menu', 'app' ) );

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat'
        ));
    }

endif; //procab_setup
add_action('after_setup_theme', 'procab_setup');


/**
 * Register widget area.
 *
 * @since procab 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function procab_widgets_init()
{
    register_sidebar(array(
        'name' => __('Widget Area', 'app'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'app'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ));
}
add_action('widgets_init', 'procab_widgets_init');
/*
 *
 */
include get_template_directory() .'/includes/shortcodes.php';
include get_template_directory ().'/includes/shortcode-functions.php';

/**
 * Enqueue scripts and styles.
 *
 * @since procab 1.0
 */
function procab_scripts(){
    wp_enqueue_style('maginific-popup', get_template_directory_uri() . '/assets/vendor/magnific-popup.css', array(), '1.0');
    wp_enqueue_style('slick', get_template_directory_uri() . '/assets/vendor/slick.css', array(), '1.0');
   // wp_enqueue_style('bundle', get_template_directory_uri() . '/assets/css/bundle.css', array(), '1.0');

    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css', array(), '1.1');
  //  wp_deregister_script('jquery');
    $mapKey = get_field('google_map_api_key','options');
    $key = !empty($mapKey)?'?key='.$mapKey:'';

    wp_register_script( 'googlemaps', 'https://maps.googleapis.com/maps/api/js'.$key, array(), '1.0', false );
//    wp_enqueue_script( 'jquery');

    if (! is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', get_template_directory_uri() . "/assets/vendor/jquery-3.3.1.min.js",array(), '3.3.1', false);
        wp_enqueue_script('jquery');
    }

    wp_register_script("custom", get_template_directory_uri() . '/assets/js/custom.js', array(), "1.0", false);
    wp_register_script("function", get_template_directory_uri() . '/assets/js/function.js', array(), "1.0", true);
    wp_register_script("nice-scroll", get_template_directory_uri() . '/scripts/vendor/jquery.nicescroll.js', array(), "1.0", true);
    wp_register_script("bundle", get_template_directory_uri() . '/assets/js/bundle.js', array(), "1.1", true);


    wp_register_script("magnific-popup", get_template_directory_uri() . '/assets/vendor/jquery.magnific-popup.min.js', array(), "1.0", false);
    wp_register_script('slick', get_template_directory_uri() . '/assets/vendor/slick.min.js', array(), '1.0', false);
    wp_register_script('bootstrap-bundle', get_template_directory_uri() . '/assets/vendor/bootstrap-4.1.1/dist/js/bootstrap.bundle.min.js', array(), '1.0', false);
    wp_enqueue_script(array(
        'googlemaps',
        'custom',
        'function',
        'nice-scroll',
        'bundle',
        'slick',
        'magnific-popup',
        'bootstrap-bundle'
    ));
}

add_action('wp_enqueue_scripts', 'procab_scripts');

/*
 * Google map API key
 */
add_action('acf/init', 'procab_acf_init');
function procab_acf_init() {
    $mapKey = get_field('google_map_api_key','options');
    $key = !empty($mapKey)?$mapKey:'';
    acf_update_setting('google_api_key', $key);
}
/*
 *  Image Type Column
 */

add_filter('manage_posts_columns', 'kd_add_image_type_columns');

function kd_add_image_type_columns($columns)
{
    unset($columns['author']);
    return array_merge($columns,
        array('image_type' => 'Image Type'));
}

add_action('manage_pages_custom_column','kd_image_custom_columns', 10, 2);

function kd_image_custom_columns($column, $post_id) {

    global $post;
   // var_dump($post);

 /*   if($post->post_type=="offer") {
        $offerImage = get_field("offer_image");

    }*/
    $image =  wp_get_attachment_image_src(get_post_thumbnail_id($post->ID));
    $thumbImg = \App\getImageManager()->resize( \App\getImageDirectoryPath($image[0]), \App\IMAGE_SIZE_THUMBNAIL);
    switch ($column) {
        case 'image_type' :
            ?>
            <img src="<?=$thumbImg?>" >
            <?php break;
    }

}

/*
 *
 */
add_action( 'manage_posts_custom_column', 'custom_page_column_content', 10, 2 );
function custom_page_column_content( $column_name, $post_id ) {
         $image =  wp_get_attachment_image_src(get_post_thumbnail_id($post_id));
        $thumbImg = \App\getImageManager()->resize( \App\getImageDirectoryPath($image[0]), \App\IMAGE_SIZE_THUMBNAIL);
        switch ($column_name) {
            case 'image_type' :
                ?>
                <img src="<?=$thumbImg?>" >
                <?php break;
    }
}



/*
* OPen external links in a new tab
*/
add_action( 'wp_head', 'app_openlinks_innnewtab' );
function app_openlinks_innnewtab() { ?>
    <script type="text/javascript">// <![CDATA[ javascript:void(0);
        jQuery(document).ready(function($){
            $('a[href]:not([href^="<?php echo site_url(); ?>"]):not([href^="#"]):not([href^="/"]):not([href*="javascript:void(0)"]):not([href^="tel"])').attr( 'target', '_blank' );
        });
        // ]]></script>
    <?php
}
/*
 *
 */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
}