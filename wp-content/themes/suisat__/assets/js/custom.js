var __winWidth = $(window).width();


function customSelect() {
    $('select.select').each(function(){
        var $this = $(this), numberOfOptions = $(this).children('option').length;
        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');
        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);
        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }
        var $listItems = $list.children('li');
        $styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });
        $listItems.click(function(e) {
            //e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');

            $this.val($(this).attr('rel'));
            $list.hide();
            $this.change();
        });
        $(document).click(function() {
            $styledSelect.removeClass('active');
            $list.hide();
        });
    });

    $('.js-block-frm-vessel').each(function(){
        var $this = $(this), numberOfOptions = $(this).children('option').length;
        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');
        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).remove());

        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);
        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }
        var $listItems = $list.children('li');
        $styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });
        $listItems.click(function(e) {
            //e.stopPropagation();
            //$styledSelect.text($(this).text()).removeClass('active');
            $styledSelect.removeClass('active');

            $this.val($(this).attr('rel'));
            $list.hide();
            $this.change();
        });
        $(document).click(function() {
            $styledSelect.removeClass('active');
            $list.hide();
        });
    });



}



function niceScroll() {
    if(__winWidth > 768) {
        $('.select-options').niceScroll({
            cursorcolor:"#2857a4",
            cursorwidth:"8px"
        });
    }else{
        $('.select-options').niceScroll().remove();
    }
}

$( document ).ready(function() {

    var __navbarToggler = $('.navbar-toggler'),
        __navCollapse = $('.nav-collapse'),
        __navbar = $('.navbar');

    __navbarToggler.on('click', function () {
        $(this).attr('aria-expanded', function (i, attr) {
            return attr == 'true' ? 'false' : 'true'
        });

        __navCollapse.toggleClass('show');

        if (__navCollapse.hasClass('show')){
            __navbar.addClass('menu-shown');
        }else{
            __navbar.removeClass('menu-shown');
        }
    });




    if(__winWidth > 768) {
        customSelect();
    }
    niceScroll();

    $('.section-post .section__image').hover(
        function(){
        $(this).siblings('.section__head').find('.link-more').css({"text-decoration": "underline"})
        }, function () {
            $(this).siblings('.section__head').find('.link-more').removeAttr('style')
        }
    );




/*    var __body = $('body'),
        __overlay = $('.overlay');

    $('.navbar-toggler').on('click', function(){
        __body.toggleClass('overflow-hidden');


        if(__body.hasClass('overflow-hidden') && __overlay.length < 1){
            __body.append('<div class="overlay"></div>');
            $('.navbar').addClass('menu-active');
        }else{
            $('.overlay').remove();
            $('.navbar').removeClass('menu-active');
        }
    });*/


/*$(document).on('click', '.overlay',  function () {
    $('.navbar-collapse').removeClass('show');
    $('.navbar-toggler').addClass('collapsed').attr("aria-expanded","false");
    __body.removeClass('overflow-hidden');
    $('.navbar').removeClass('menu-active');
    $(this).remove();
})*/
});


$(window).resize(function () {
    __winWidth = $(window).width();
    niceScroll();
/*    if(__winWidth > 768) {
        customSelect();
    }*/
})
