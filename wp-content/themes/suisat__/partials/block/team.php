<ul class="list-images">
    <?php
    $query = new WP_Query( $args );
    // var_dump($query);
    if($query->have_posts()):
        while($query->have_posts()): $query->the_post();
            //get_template_part('partials/news-item');
            ?>
            <li class="team-list">
                <?php
                // news-thumb
                $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
                if($image) { $img = \App\getImageManager()->resize( \App\getImageDirectoryPath($image[0]), \App\IMAGE_SIZE_BLOCK_THUMB); }
                $surname = get_field('surname');
                $mailAddress = get_field('mail_address');
                $job = get_field('job');
                ?>
                <a onclick="" href="javascript:void(0)" style="background-image: url(<?php echo $img; ?>)">
                    <img src="<?php echo $image[0]; ?>">
                    <div class="content-wrap">
                        <?php the_title('<h2>','</h2>'); ?>
                        <span class="team-position"> <?php echo $mailAddress.'<br/>'.$job ; ?></span>
                    </div>
                </a>
            </li>

        <?php
        endwhile;
    else :
        echo 'Content does not match the filter criteria';
    endif;
    ?>
</ul>