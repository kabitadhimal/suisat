<?php
$args = [ 'post_type' => 'team','posts_per_page' => -1,'orderby'=>'menu_order', 'order'=>'ASC','post_status'=> 'publish'];
if(isset($_GET['cat']))  $teamCategory = (int) $_GET['cat'];

$departTerms = get_terms( 'department', array(
    'hide_empty' => false,
    'orderby' => 'name'
));

if(!empty($teamCategory) && $teamCategory !='undefined') :
    $term = get_term( $teamCategory, 'department' );
    $taxQuery = [
        'taxonomy' => 'department',
        'field'    => 'term_id',
        'terms'    => $teamCategory,
    ];
    $args['tax_query'] = [$taxQuery];
    ?>

    <div class="team-members">
        <h1><?=$term->name?></h1>
        <?php include(locate_template("partials/block/team.php")); ?>
    </div><!-- /.list-images -->

<?php
    else :
        foreach($departTerms as $category):
        if($teamCategory){
        $termID = $inputDepartCategories;
        $term = get_term( $termID, 'department' );
        $catName = $term->name;
        }else {
        $termID = $category->term_id;
        $catName =  $category->name;
        }

        $taxQuery = [
        'taxonomy' => 'department',
        'field'    => 'term_id',
        'terms'    => $termID,
        ];

        $args['tax_query'] = [$taxQuery];
        ?>
        <div class="team-members">
            <h1><?=$catName?></h1>
            <?php include(locate_template("partials/block/team.php")); ?>
        </div><!-- /.list-images -->
        <?php endforeach;?>
<?php endif;