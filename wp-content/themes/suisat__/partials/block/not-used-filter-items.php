<?php
$actionUrl = get_permalink();
//build query
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = [ 'post_type' => 'fleet', 'paged' => $paged, 'paged' => $paged,'posts_per_page' => -1,'post_status'=> 'publish'];
$taxQuery = [];
$searchQuery = '';
$inputSizeCategories = '';
if(isset($_GET['size'])) $inputSizeCategories = (int) $_GET['size'];
if($inputSizeCategories){
$taxQuery = [
'taxonomy' => 'size',
'field'    => 'term_id',
'terms'    => $inputSizeCategories,
];

$args['tax_query'] = [$taxQuery];
}

$inputTypeCategories = '';
if(isset($_GET['type'])) $inputTypeCategories = (int) $_GET['type'];
if($inputTypeCategories){
$taxQuery = [
'taxonomy' => 'type',
'field'    => 'term_id',
'terms'    => $inputTypeCategories,
];
$args['tax_query'] = [$taxQuery];
}
    $query = new WP_Query( $args );
    while($query->have_posts()): $query->the_post();
    ?>
    <li>
        <?php
        $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
        if($image) { $img = \App\getImageManager()->resize( \App\getImageDirectoryPath($image[0]), \App\IMAGE_SIZE_BLOCK_THUMB); }
        ?>
        <a  href="<?php the_permalink(); ?>" style="background-image: url(<?php echo $img; ?>)">
            <img src="<?php echo $image[0]; ?>">
            <?php the_title('<h2>','</h2>'); ?>
        </a>
    </li>

<?php   endwhile;
    wp_reset_postdata();