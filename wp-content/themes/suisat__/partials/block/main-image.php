<?php
$image = get_field('block_image');
$title = get_field('block_title');
$subTitle = get_field('block_subtitle');
if(!empty($image)): ?>
<section class="block block--hero promo--bkg text-center position-relative is-extended-full wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="slider__b">
        <figure class="slider__pic mb-0 text-center"><img alt="Hero Slider Image" src="<?=$image?>" /></figure>
        <div class="promo__c is-floated center">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="card text-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                        <?php if(!empty($title)) : ?><div class="h1 fw-r mb-0"><?=$title?></div><?php endif; ?>
                        <?php if(!empty($subTitle)) : ?><p class="text-white"><?=$subTitle?></p><?php endif; ?>

                    </div>
                </div>
            </div>
        </div><!-- /.Prom content ends -->
    </div>
</section><!-- /.Home page hero slider ends -->
<?php endif;