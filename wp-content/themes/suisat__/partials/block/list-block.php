<?php $introBlock = get_field('introduction_block'); ?>
<!-- Reusable simple text block -->
<section class="block block--text text-banner shadowed gap-p-eq bg-white text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="text-container--sm">
        <?php if(!empty($introBlock['title'])): ?>
            <header class="block__h">
                <h2 class="mb-2"><?=$introBlock['title']?></h2>
            </header>
        <?php endif; ?>
        <?php if(!empty($introBlock['text'])): ?>
            <div class="block__b">
                <?=$introBlock['text']?>
            </div>
        <?php endif; ?>
    </div>
</section><!-- /.Reusable simple text block ends -->
<!-- Reusable simple text block -->
<section id="section1" class="block block--text gap-p-eq above-content top is-extended top bottom wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div id="block-media-grid" class="row">
            <?php
            $listItems = get_field('lists');
            foreach ($listItems as $items):
                $link = $items['link'];
                ?>
                <div class="col-sm-4 mb-3 mb-sm-3">
                    <div class="card shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <?php if(!empty($items['image'])): ?>
                            <figure class="card__pic mb-0"><img alt="Post Thumbnail" class="img img-full img-fluid" src="<?php echo $items['image']; ?>" /></figure>
                        <?php endif; ?>
                        <div class="card__b text-center small-line-height">
                            <?php if(!empty($items['title'])): ?>
                                <p class="sub-title middle-line"><?=$items['title']?></p>
                            <?php endif; ?>
                            <?php if(!empty($items['subtitle'])): ?>
                                <h3 class="line-stack middle"><?=$items['subtitle']?></h3>
                            <?php endif; ?>
                            <?php if(!empty($items['description'])): ?>
                                <?=$items['description']?>
                            <?php endif; ?>
                        </div>
                        <?php if(!empty($link)): ?>
                            <a href="<?=$link?>" class="link"></a>

                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>