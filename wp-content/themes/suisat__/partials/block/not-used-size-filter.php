<?php
//build query
$args = [ 'post_type' => 'fleet','order' =>'', 'orderby'=>'post_date','posts_per_page' => -1,'post_status'=> 'publish'];
$inputSizeCategories = '';
if(isset($_GET['size'])) $inputSizeCategories = (int) $_GET['size'];
if($inputSizeCategories){
    $taxQuery = [
        'taxonomy' => 'size',
        'field'    => 'term_id',
        'terms'    => $inputSizeCategories,
    ];

    $args['tax_query'] = [$taxQuery];
}
//Precaution -> News shares the same file
include(locate_template("partials/block/ship-list.php"));