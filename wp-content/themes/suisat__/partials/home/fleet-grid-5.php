<?php
$firstRow = get_field('fleet_first_row');
$firstImage = $firstRow['image'];
$firstTitle = $firstRow['title'];

$secondRow = get_field('fleet_second_row');
$secondImage1 = $secondRow['image_1'];
$secondImage2 = $secondRow['image_2'];
$secondTitle = $secondRow['title'];

$thirdRow = get_field('fleet_third_row');
$thirdImage1 = $thirdRow['image_1'];
$thirdImage2 = $thirdRow['image_2'];

$thirdTitle = $thirdRow['title'];


$fourthRow = get_field('fleet_fourth_row');
$fourthImage = $fourthRow['image'];
$fourthTitle = $fourthRow['title'];
$fourthLink = !empty($fourthRow['button_link'])? $fourthRow['button_link']:"";


$seeAllButton = get_field('fleet_grid_button');



$seeFleetLink ='<a href="'.$seeAllButton['url'].'" class="link-more">'. $seeAllButton['title'].'</a>';

?>

<div class="slider slider--mobile">
    <div class="slider__slides">
        <div class="slider__slide">
            <div class="slide__head">
                <?php if(!empty($firstTitle)):
                ?>
                <h2><?=$firstTitle?></h2>
                <?php endif; ?>
                <?=$seeFleetLink?>
            </div><!-- /.slide__head -->
            <?php if(!empty($firstImage)): ?><div class="slide__image no-repeat" style="background-image: url(<?php echo $firstImage; ?>)"></div><?php endif; ?>
        </div><!-- /.slider__slide -->

        <div class="slider__slide">
            <div class="slide__head">
               <?php if(!empty($secondTitle)): ?> <h2><?=$secondTitle?></h2><?php endif; ?>
                <?=$seeFleetLink?>
            </div><!-- /.slide__head -->

            <?php if(!empty($secondImage1)): ?><div class="slide__image  no-repeat" style="background-image: url(<?php echo $secondImage1; ?>)"></div><?php endif; ?>
        </div><!-- /.slider__slide -->

        <div class="slider__slide">
            <div class="slide__head">
                <?php if(!empty($thirdTitle)): ?> <h2><?=$thirdTitle?></h2><?php endif; ?>
                <?=$seeFleetLink?>
            </div><!-- /.slide__head -->
            <?php if(!empty($thirdImage1)): ?><div class="slide__image  no-repeat" style="background-image: url(<?php echo $thirdImage1; ?>)"></div><?php endif; ?>
        </div><!-- /.slider__slide -->

        <div class="slider__slide">
            <div class="slide__head">
                 <?php if(!empty($fourthTitle)): ?>  <h2><?=$fourthTitle?></h2><?php endif; ?>
                <?=$seeFleetLink?>
            </div><!-- /.slide__head -->

            <?php if(!empty($fourthImage)): ?> <div class="slide__image" style="background-image: url(<?php echo $fourthImage; ?>)"></div><?php endif; ?><!-- /.slide__image -->
        </div><!-- /.slider__slide -->
    </div><!-- /.slider__slides -->
</div><!-- /.slider -->

<section class="section section--grid hidden-xs">
    <div class="cols">
        <div class="cols__row cols__row--blue">
            <div class="container container--full">
                <?php if(!empty($firstImage)): ?>
                    <div class="col col--size2 no-repeat" style="background-image: url(<?php echo $firstImage; ?>)"></div><!-- /.col col-/-1of3 -->
                <?php endif;

                if(!empty($firstTitle)):
                ?>
                    <div class="col col--size2 pl-10">
                        <h2><?=$firstTitle?></h2>
                    </div><!-- /.col col-/-1of3 -->
                <?php endif; ?>

               <!-- <div class="col col--1of3"></div>--><!-- /.col col-/-1of3 -->
            </div><!-- /.container -->
        </div><!-- /.cols__row -->

        <div class="cols__row">
            <div class="container container--full">
                <?php if(!empty($secondTitle)): ?>
                    <div class="col col--size2">
                        <h2><?=$secondTitle?></h2>
                    </div><!-- /.col col-/-1of3 -->
                <?php endif; ?>

              <?php if(!empty($secondImage1)): ?>  <div class="col col--size3  no-repeat scale" style="background-image: url(<?php echo $secondImage1; ?>)"></div><?php endif; ?>
                <?php if(!empty($secondImage2)): ?> <div class="col col--size2  no-repeat" style="background-image: url(<?php echo $secondImage2; ?>)"></div><?php endif; ?>
            </div><!-- /.container -->
        </div><!-- /.cols__row -->

        <div class="cols__row cols__row--grey">

            <?php if(!empty($thirdImage1)): ?> <div class="col col--size2  no-repeat" style="background-image: url(<?php echo $thirdImage1; ?>)"></div><?php endif; ?><!-- /.col col-/-1of3 -->
            <?php if(!empty($thirdTitle)): ?> <div class="col col--size3"><h2><?=$thirdTitle?></h2> </div><?php endif; ?>
            <?php if(!empty($thirdImage2)): ?>  <div class="col col--size2  no-repeat" style="background-image: url(<?php echo $thirdImage2; ?>)"></div><?php endif; ?><!-- /.col col-/-1of3 -->
        </div><!-- /.cols__row -->

        <div class="cols__row cols__row--grey">
            <div class="container">
                <?php if(!empty($fourthTitle)): ?>
                    <div class="col col--1of3 pl-2">
                        <h2><?=$fourthTitle?></h2>
                    </div><!-- /.col col-/-1of3 -->
                <?php endif; ?>

                <?php if(!empty($fourthImage)): ?>  <div class="col col--1of3  no-repeat" style="background-image: url(<?php echo $fourthImage; ?>)"></div><?php endif; ?><!-- /.col col-/-1of3 -->
                <div class="col col--1of3 see-more">
                    <a class="absolute-link" href="<?=$seeAllButton['url']?>"></a>
                    <?php $seeAllButton = explode(' ',$seeAllButton['title'], 2);?>
                    <h2>
                        <?=$seeAllButton[0]?> <br><?=$seeAllButton[1]?>
                    </h2>
              </div><!-- /.col col-/-1of3 -->
            </div><!-- /.container -->
        </div><!-- /.cols__row -->
    </div><!-- /.cols -->
</section><!-- /.section -->