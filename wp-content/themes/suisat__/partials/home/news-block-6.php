<?php
    $args = array(
        'post_type' => 'post',
        'order' => 'DESC',
        'orderby'=> 'post_date',
        'ignore_custom_sort' => TRUE,
        'posts_per_page' => 1,
        'post_status' => 'publish'
    );
    $query = new WP_Query( $args );
    while ($query->have_posts()): $query->the_post();
    ?>
        <section class="section-post">
            <div class="container container--no-padding">
                <div class="section__head">
                    <h5>News - <?php  the_date('d/m/Y'); ?></h5>
                    <?php the_title('<h3>','</h3>'); ?>
                    <a href="<?php the_permalink(); ?>" class="link-more">Learn more</a>
                </div><!-- /.section__head -->
                <div class="section__image">
                    <?php
                    $photo = get_the_post_thumbnail_url($post->ID, 'full');
                    if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_HOME_THUMBNAIL);
                    ?>
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo $photo; ?>" alt="image"></a>
                </div><!-- /.section__image -->
            </div><!-- /.container -->
        </section><!-- /.section -->
        <?php
    endwhile;wp_reset_postdata();