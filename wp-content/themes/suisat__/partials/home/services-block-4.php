<?php
$serviceTitle = get_field("service_title");
$link = get_field("all_services");

$servicesSlider = get_field('services_slider');
if(!empty($servicesSlider)):
    foreach ($servicesSlider as $slider) {
        $title = $slider['title'];
        $text = $slider['text'];
        $icon = $slider['icon'];
    }
endif;
?>
<section class="section-services">
    <div class="container container--no-padding">
        <header class="section__head">
            <h2>
                <?php if(!empty($serviceTitle)) : echo $serviceTitle; endif; ?>

            </h2>
            <?php if(!empty($link)):
                if(!empty($link['target'])) :
                    $target='target="_blank"';
                endif;
                ?>
                        <a href="<?=$link['url']?>" <?=$target?> class="link-more"><?=$link['title']?></a>
                    <?php
                endif;?>
        </header><!-- /.section__head -->

        <div class="slider">
            <div class="slider__slides">
                <?php
                if(!empty($servicesSlider)):
                foreach ($servicesSlider as $slider) {
                    $title = $slider['title'];
                    $text = $slider['text'];
                    $icon = $slider['icon'];
                    $image = $slider['image'];
                    $link = !empty($slider['link']) ? $slider['link'] :'#';
                     if(!empty($link)):
                        if(!empty($link['target'])) :
                            $target='target="_blank"';
                        endif;
                     endif;
                ?>
                <div class="slider__slide">
                        <div class="service" style="background-image: url(<?php echo $image; ?>)">
                        <a href="<?=$link['url']?>" <?=$target?> >
                            <div class="content-wrap">
                                <div class="title">
                                  <!--  <i class="<?/*=$icon*/?>"></i>-->
                                    <img src="<?=$icon?>" >
                                    <?php if(!empty($title)) : echo '<h5>'.$title.'</h5>'; endif; ?>
                                </div>
                                <div class="description">
                                    <?php if(!empty($text)) : echo $text; endif; ?>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.service -->
                </div><!-- /.slider__slide -->
                <?php } endif; ?>
            </div><!-- /.slider__slides -->
        </div><!-- /.slider -->
    </div><!-- /.container -->
</section><!-- /.section-/-services -->