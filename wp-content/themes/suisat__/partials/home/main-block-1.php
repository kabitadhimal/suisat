<?php
    $url = get_field("video_url");
    $photo = get_field("home_image");
    $homeLogo = get_field("home_logo");
    $homeTitle = get_field("home_title");
    $homeSubtitle = get_field("home_subtitle");

    if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_HOME_THUMBNAIL);
    $youtubeID = app\getYoutubeVideoID($url);
    $start = app\getStartTimeYoutubeVideo($url);

?>
<div class="intro" style="background-image: url(<?php echo $photo; ?>)">
    <div class="intro__video">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$youtubeID?>?autoplay=1<?=$start?>&amp;loop=1&amp;mute=1&amp;rel=0&amp;playlist=<?=$youtubeID?>" allowfullscreen=""></iframe>
    </div><!-- /.intro__video -->

    <div class="intro__content">
		<div class="intro__content-inner">
			<?php
			if ( $photo ) : ?>
				<a class="navbar-brand" href="#">
					<img alt="<?php echo !empty($photo['alt']) ? $photo['alt'] : ''; ?>" class="navbar-brand-logo" src="<?php echo $homeLogo; ?>">
				</a>
			<?php endif;
			?>
			<hgroup class="intro-title-group">
				<h2 class="intro__title text-uppercase fw-medium mb-0"><?php echo $homeTitle ?></h2>
				<div class="intro__subtitle text-light fw-light"><?php echo $homeSubtitle ?></div>
			</hgroup>
		</div>
    </div><!-- /.intro__content -->
</div><!-- /.intro -->