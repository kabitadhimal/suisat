<?php
$bgColor = $block['background_color'];
$backGroundColor ="bg-".$bgColor;
?>
<section class="section <?=$backGroundColor?>">
    <div class="container container--no-padding">
        <ul class="list-images list-gallery">
            <?php $galleryImages = $block["gallery_images"];
            if(!empty($galleryImages)):
                foreach ($galleryImages as $image):
                    $title = $image['title'];
                    $fullPhoto = $image['url'];
                    if($fullPhoto): $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($fullPhoto), \App\IMAGE_SIZE_GALLERY); endif; ?>
                    <li>
                        <a class="image-popup-vertical-fit" href="<?php echo $fullPhoto; ?>" style="background-image: url(<?php echo $photo; ?>)">
                            <img src="<?php echo $photo; ?>" alt="<?=$title?>">
                            <?php /* if(!empty($title)): ?>
                                <h2><?=$title?></h2>
                            <?php endif; */?>
                        </a>
                    </li>
                <?php endforeach;
            endif;
            ?>
        </ul><!-- /.list-images -->
    </div><!-- /.container -->
</section>