<?php
$firstRow = $block['first_row'];
$secondRow = $block['second_row'];
$bgColor1 = $firstRow['background_color'];
$bgColor2 =$secondRow['background_color'];
$backGroundColor1 ="bg-".$bgColor1;
$backGroundColor2 ="bg-".$bgColor2;
?>
<section class="section">
    <?php if(!empty($firstRow['text'])  && !empty($firstRow['image'])): ?>
        <div class="row">
            <div class="col-sm-6 col--empty" style="background-image: url(<?php echo $firstRow['image']; ?>)">
            </div><!-- /.col-sm-6 -->
            <div class="col-sm-6 <?=$backGroundColor1?>">
                <div class="section__entry">
                    <?=$firstRow['text']?>
                    <?php $link = $firstRow['link'];
                    if(!empty($link)):
                        $target = !empty($link['target'])? 'target="_blank"':"";
                        ?>
                        <a href="<?php echo $link['url']; ?>" <?=$target?> class="link-more"><?php echo $link['title']; ?></a>
                    <?php endif; ?>
                </div><!-- /.section__entry -->
            </div><!-- /.col-sm-6 -->
        </div><!-- /.row -->
    <?php endif; ?>
    <?php if(!empty($secondRow['text']) && !empty($secondRow['image'])): ?>
        <div class="row">
            <div class="col-sm-6 <?=$backGroundColor2?>">
                <div class="section__entry">
                    <?=$secondRow['text']?>
                    <?php $link = $secondRow['link'];
                    if(!empty($link)):
                        $target = !empty($link['target'])? 'target="_blank"':"";
                        ?>
                        <a href="<?php echo $link['url']; ?>"  <?=$target?> class="link-more"><?php echo $link['title']; ?></a>
                    <?php endif; ?>
                </div><!-- /.section__entry -->
            </div><!-- /.col-sm-6 -->
            <div class="col-sm-6 col--empty" style="background-image: url(<?php echo $secondRow['image']; ?>)">
            </div><!-- /.col-sm-6 -->
        </div><!-- /.row -->
    <?php endif; ?>
</section><!-- /.section -->