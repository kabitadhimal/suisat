<?php
$colContents = $block['column_contents'];
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>
<section class="section <?php echo $section_gap_type; ?>">
    <div class="section__body">
        <div class="container container--no-padding">
            <div class="card-group">
            <?php foreach ($colContents as $content):
                $bgColor = $content['background_color'];
                $backGroundColor ="bg-".$bgColor;
                ?>
                <div class="card  <?=$backGroundColor?>">
                    <div class="card__image">
                        <img src="<?php echo $content['image']; ?>" alt="image">
                    </div><!-- /.card__image -->
                    <div class="card-body">
                        <?php if($content['title']): ?>
                        <h4 class=""><?=$content['title']?></h4>
                    <?php endif;
                       if($content['text']):
                            echo $content['text'];
                        endif;
                        ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div><!-- /.container -->
    </div><!-- /.section__body -->
</section>