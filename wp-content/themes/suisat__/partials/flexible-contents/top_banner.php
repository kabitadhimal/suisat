<?php
$photo = $block['image'];
$title = $block['title'];
$url = $block['video_url'];
if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_TOP_BANNER);
$youtubeID = app\getYoutubeVideoID($url);
$start = app\getStartTimeYoutubeVideo($url);
if(!empty($photo)):
    ?>
    <div class="intro" style="background-image: url(<?php echo $photo; ?>)">
        <?php if(!empty($youtubeID)): ?>
            <div class="intro__video">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$youtubeID?>?autoplay=1<?=$start?>&amp;loop=1&amp;mute=1&amp;rel=0&amp;playlist=<?=$youtubeID?>" allowfullscreen=""></iframe>
            </div><!-- /.intro__video -->
        <?php endif; ?>
        <?php if(!empty($title)): ?>
            <div class="intro__content">
                <h2><?=$title?></h2>
            </div><!-- /.intro__content -->
        <?php endif; ?>
    </div><!-- /.intro -->
<?php endif;