<?php
    $googleMap = $block['google_map'];
?>
<section class="section">
    <div class="map" id="google-map" data-lat="<?=$googleMap['lat']?>" data-lng="<?=$googleMap['lng']?>" data-marker="<?php echo get_template_directory_uri(); ?>/assets/images/temp/map-icon-new.png"></div>
</section>

