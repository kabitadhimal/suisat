<?php
$title = $block['title'];
$text = $block['text'];
$linkLabel = $block['link_label'];
$linkUrl = $block['link_url'];
$slider = $block['slider_images'];
$countImg = count($slider);
$i=0;
?>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        while( $i < $countImg) {
            $activeClass = ($i == 0 ? 'class="active"' : ''); ?>
            <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>" <?=$activeClass?>></li>
            <?php $i++;
        } ?>
    </ol>
    <div class="carousel-inner">
        <?php
        $i=0;
        foreach ($slider as $image) {
        $photo = $image['image'];
        $activeClass = ($i == 0 ? 'active' : '');
        if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_VISION_SLIDER);
        ?>
        <div class="carousel-item <?=$activeClass?>">
            <img class="d-block w-100" src="<?=$photo?>" alt="slide">
            <div class="carousel-caption d-none d-md-block">
                <?php if(!empty($title)): ?>
                <h2>
                    <i class="ico-circle"></i>
                    <?=$title?>
                </h2>
                <?php endif; ?>
                <?php if(!empty($text)): ?><?=$text?><?php endif; ?>
            </div>
        </div>
        <?php $i++; } ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <?php if(!empty($linkUrl)): ?>
        <a href="<?=$linkUrl?>" class="link-more"><?=$linkLabel?></a>
    <?php endif; ?>
</div>

