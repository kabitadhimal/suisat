<?php
$contents = $block['columns_content'];
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>

<section class="section section--simple <?php echo $section_gap_type; ?>">
    <div class="container container--no-padding">
        <ul class="services">
            <?php
            foreach ($contents as $content):
            ?>
            <li class="service" style="background-image: url(<?=$content['image']?>)">
                <a href="#">
                    <i class="ico-boat"></i>
                    <?php if(!empty($content['logo_title'])): ?><h5><?=$content['logo_title']?></h5> <?php endif; ?>

                    <?php if(!empty($content['roll_over_text'])): ?>
                        <div class="service__overlay">
                            <?=$content['roll_over_text']?>
                        </div>
                    <?php endif; ?>
                </a>
            </li><!-- /.service -->
            <?php endforeach; ?>

        </ul><!-- /.services -->

    </div><!-- /.container -->
</section><!-- /.section -->