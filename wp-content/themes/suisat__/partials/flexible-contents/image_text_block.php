<?php
$title = $block['title'];
$image = $block['image'];
$link = $block['link'];
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>
<section class="section-post section--simple m-t-0 <?php echo $section_gap_type; ?>">
    <div class="container container--no-padding">
        <div class="section__head">
            <?php if(is_single()) : ?>
                <h5><?php  the_date('d/m/Y'); ?></h5>
            <?php endif; ?>
            <?php if(!empty($title)): ?>
                <h3><?=$title?></h3>
            <?php endif; ?>
            <?php if(!empty($link)):
                $linkUrl=$link['url'];
                ?>
                <a href="<?=$linkUrl?>" class="link-more"><?=$link['title']?></a>
            <?php endif; ?>
        </div><!-- /.section__head -->
        <?php if(!empty($image)): ?>
            <div class="section__image">
                <?php if(!empty($link)):
                    ?>
                    <a href="<?=$linkUrl?>">
                        <img src="<?=$image?>" alt="image"></a>
                <?php else : ?>
                    <img src="<?=$image?>" alt="image">
                <?php endif; ?>
            </div><!-- /.section__image -->
        <?php endif; ?>
    </div><!-- /.container -->
</section><!-- /.section -->