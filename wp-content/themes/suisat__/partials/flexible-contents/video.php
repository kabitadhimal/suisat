<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 6/5/2018
 * Time: 3:23 PM
 */

$videoType = $block['type'];
$url = $block['video_url'];
$youtubeID = app\getYoutubeVideoID($url);
$start = app\getStartTimeYoutubeVideo($url);

if($videoType=="embeded-video") {
    ?>
    <div class="section section--simple">
        <div class="container">
            <div class="video">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$youtubeID?>?<?=$start?>&amp;rel=0&amp;playlist=<?=$youtubeID?>" allowfullscreen=""></iframe>
            </div><!-- /.video -->
        </div><!-- /.container -->
    </div><!-- /.section -->
    <?php
}else {
    ?>
    <div class="section">
        <div class="video">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$youtubeID?>?<?=$start?>&amp;rel=0&amp;playlist=<?=$youtubeID?>" allowfullscreen=""></iframe>
        </div><!-- /.video -->
    </div><!-- /.section -->
    <?php
}