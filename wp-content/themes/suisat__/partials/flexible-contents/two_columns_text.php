<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 10/11/2017
 * Time: 3:33 PM
 */
$blockGroup = $block['column_contents'];
$bgColor = $block['background_color'];
$title = $block['title'];
$backGroundColor ="bg-".$bgColor;
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>
<section class="section section--simple <?=$backGroundColor?> <?php echo $section_gap_type; ?>">
    <div class="container container--no-padding">
        <div class="row row--bottom align-items-start">
            <div class="col-sm-12 title">
                <?php if(!empty($title)): ?>
                    <h2><?=$title?></h2>
                <?php endif; ?>
            </div>
            <div class="col-sm-6">
                <?php if(!empty($blockGroup['col_1'])):
                    echo $blockGroup['col_1'];
                 endif; ?>
            </div><!-- /.col-sm-6 -->
            <?php if(!empty($blockGroup['col_1'])): ?>
                <div class="col-sm-6"><?php echo $blockGroup['col_2'] ?></div>
            <?php endif; ?>
            </div><!-- /.col-sm-6 -->
    </div><!-- /.row no-gutters -->
</section>
