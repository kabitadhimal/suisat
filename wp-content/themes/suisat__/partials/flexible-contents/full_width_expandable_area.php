<?php
$contents = $block['contents'];
?>
<section class="section">
    <div class="accordion--secondary text-center" id="accordion2">
        <?php
        $count= 4;
        foreach ($contents as $content):
            $show ='';
            $areaExpand = "false";
            $collapse = "collapsed";
            $title = $content['title'];
            $text = $content['text'];
            $col1 = $content['col_1'];
            $col2 = $content['col_2'];
            $col3 = $content['col_3'];

            if($count==4) {
                $show = "show";
                $areaExpand = "true";
                $collapse ="";
            }
        ?>

        <div class="accordion__section">
            <div id="heading<?=$count?>">
                <h2 class="mb-0">
                    <button class="btn-link <?=$collapse?>" type="button" data-toggle="collapse" data-target="#collapse<?=$count?>" aria-expanded="<?=$areaExpand?>" aria-controls="collapse<?=$count?>">
                       <?php if(!empty($title)): echo $title ; endif; ?>
                </h2>
            </div>
            <div id="collapse<?=$count?>" class="collapse <?=$show?>" aria-labelledby="heading4" data-parent="#accordion2">
                <div class="container container--no-padding">
                <?php if(!empty($text)): echo $text; endif; ?>
                    <?php if(!empty($content['contains_column'])) : ?>
                    <div class="card-group">
                        <div class="card">
                            <div class="card__image">
                                <img src="<?php echo $col1['image']; ?>" alt="">
                            </div><!-- /.card__image -->
                            <div class="card-body">
                                <h3 class="card-title"><?=$col1['title']?></h3>
                                <p class="card-text"><?=$col1['text']?></p>
                                    <?php
                                    $link1 = $col1['link'];
                                    if(!empty($link1)):
                                        if(!empty($link1['target'])) :
                                        $target='target="_blank"';
                                        endif;
                                    ?>
                                        <a href="<?=$link1['url']?>" <?=$target?> class="link-more"><?=$link1['title']?></a>
                                    <?php  endif; ?>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card__image">
                                <img src="<?php echo $col2['image']; ?>" alt="">
                            </div><!-- /.card__image -->
                            <div class="card-body">
                                <h3 class="card-title"><?=$col2['title']?></h3>
                                <p class="card-text"><?=$col2['text']?></p>
                                <?php
                                $link2 = $col2['link'];
                                if(!empty($link2)):
                                    if(!empty($link2['target'])) :
                                        $target='target="_blank"';
                                    endif;
                                    ?>
                                    <a href="<?=$link2['url']?>" <?=$target?> class="link-more"><?=$link2['title']?></a>
                                <?php  endif; ?>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card__image">
                                <img src="<?php echo $col2['image']; ?>" alt="">
                            </div><!-- /.card__image -->
                            <div class="card-body">
                                <h3 class="card-title"><?=$col3['title']?></h3>
                                <p class="card-text"><?=$col3['text']?></p>

                                <?php
                                $link3 = $col3['link'];
                                if(!empty($link3)):
                                    if(!empty($link3['target'])) :
                                        $target='target="_blank"';
                                    endif;
                                    ?>
                                    <a href="<?=$link3['url']?>" <?=$target?> class="link-more"><?=$link3['title']?></a>
                                <?php  endif; ?>

                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div><!-- /.container container-/-no-padding -->
            </div>
        </div><!-- /.accorcion__section -->
        <?php $count++; endforeach; ?>
    </div>
</section><!-- /.section -->