<?php
$bgColor = $block['background_color'];
$backGroundColor ="bg-".$bgColor;
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>
<section class="section section--simple <?php echo $section_gap_type; ?>">
    <div class="section__bar text-center <?=$backGroundColor?>">
        <div class="container">
            <?php if(!empty($block['title'])): ?>
                <h2><?=$block['title']?></h2>
            <?php endif; ?>
            <?php if(!empty($block['text'])): ?>
                <?=$block['text']?>
            <?php endif; ?>
        </div><!-- /.container -->
    </div><!-- /.section__bar -->
</section>