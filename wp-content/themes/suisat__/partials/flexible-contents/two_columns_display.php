<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 10/11/2017
 * Time: 3:33 PM
 */
$blockGroup = $block['text'];
$bgColor = $block['background_color'];
$backGroundColor ="bg-".$bgColor;
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>

<section class="section section--simple <?=$backGroundColor?> <?php echo $section_gap_type; ?>">
    <div class="container container--no-padding">
        <div class="row align-items-center">
            <?php if(!empty($blockGroup)) : ?>
            <div class="col-sm-6">
                <div class="section__inner">
                    <?php if(!empty($blockGroup['title'])) : ?>
                        <h2><?php echo $blockGroup['title']; ?> </h2>
                    <?php endif; ?>

                    <?php if(!empty($blockGroup['text'])) :
                            echo $blockGroup['text'];
                     endif; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if(!empty($block['image'])): ?>
            <div class="col-sm-6">
                <div class="section__image">
                    <img src="<?php echo $block['image']; ?>" alt="two-col-image">
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>

