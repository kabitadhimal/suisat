import { $win } from '../main';

const fixedHeader = () => {
	const headerIsFixed = $win.scrollTop() >= 1; 
		
	$('.wrapper').toggleClass('navbarFixed', headerIsFixed)
}

export default fixedHeader;