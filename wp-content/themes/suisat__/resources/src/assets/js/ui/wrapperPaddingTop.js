const wrapperPaddingTop = () => {
	if ( $('.wrapper--interior').length ) {
		const headerHeight = $('.navbar').outerHeight();
		
		$('.wrapper').css('padding-top', headerHeight);
	}
}

export default wrapperPaddingTop;