import { $win } from '../main';

const overlays = () => {
	var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    }
	};


	if (isMobile.any()) {
		
		$(document).on('click', '.service a, .list-images a', function (e) {
	        var $this = $(this);
			if ( !$this.hasClass('js-show') ) {				
		        e.preventDefault();
		        
		        $this.addClass('js-show');
		        
		        $('body, html').find('.service a, .list-images a').not($this).removeClass('js-show');
			}
		})
	} 
}

export default overlays;