export const $win = $(window);
export const $doc = $(document);

import 'popper.js';
import fixedHeader from './ui/fixed-header';
import slickSlider from './ui/slickSlider';
import gallery from './ui/gallery';
import googleMap from './ui/googleMap';
import popup from './ui/popup';
import overlays from './ui/overlays';

$win.on('load', function () {
	overlays();
	popup();
	googleMap();
	gallery();
	slickSlider();
});

$(".carousel").on("touchstart", function(event){
        var xClick = event.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function(event){
        var xMove = event.originalEvent.touches[0].pageX;
        if( Math.floor(xClick - xMove) > 5 ){
            $(this).carousel('next');
        }
        else if( Math.floor(xClick - xMove) < -5 ){
            $(this).carousel('prev');
        }
    });
    $(".carousel").on("touchend", function(){
            $(this).off("touchmove");
    });
});

$win.on('load scroll', function () {
	fixedHeader();
});
