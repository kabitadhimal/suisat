<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div class="intro" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/temp/intro-2.jpg)">
</div>

<section class="section section--simple ">
	<div class="section__bar text-center bg-white">
		<div class="container">
			<h2>Oups! Page not found</h2>
			<p>The page you are looking for cannot be found.<br/>Go home by <a href="http://server.procab-dev.com/~suisat/newsuisat/">clicking here</a>!</p>
		</div><!-- /.container -->
	</div><!-- /.section__bar -->
</section>