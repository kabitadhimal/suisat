// Map module initialization
;(function() {
    //Load Google map
    loadMap($('.c-map__node'));

    //Function to load map on event details page
    function loadMap($map) {
        if ($map.length === 0) return;
        $map.each(function(index, elem) {
            var $self = $(this),
                latLngDataVal = $self.attr('data-map-latlan'),
                mapLatLanArray = latLngDataVal.split(", "),
                zoomLevel = $self.attr('data-zoom-level') * 1;
            var mapLatLan = new google.maps.LatLng(mapLatLanArray[0], mapLatLanArray[1]);

            // Define a map
            var map;

            function initialize() {
                var mapOptions = {
                    scrollwheel: false,
                    center: mapLatLan,
                    zoom: zoomLevel,
                    disableDefaultUI: false,
                    mapTypeControl: false,
                    streetViewControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(elem, mapOptions);

                //map marker
                // var marker = new MarkerWithLabel({
                //     position: mapLatLan,
                //     map: map,
                //     draggable: false,
                //     raiseOnDrag: true,
                //     labelContent: mapAddress,
                //     labelAnchor: new google.maps.Point(-10, -3),
                //     labelClass: "map-label", // the CSS class for the label
                //     labelInBackground: false,
                // });
                
                //map resize event
                var resizeTimer = null;
                google.maps.event.addDomListener(window, 'resize', function() {
                    if (resizeTimer) clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function() {
                        map.setCenter(mapOptions.center);
                    }, 10);
                });

                google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
                    // map loaded fully
                    animateMap($self)
                });
            }

            function animateMap($map) {
                $map.closest('.c-map__holder').find('.c-map__marker').addClass('animated');
            }

            // Load map on google library load completes
            google.maps.event.addDomListener(window, 'load', initialize);

        });
    }
})(jQuery);