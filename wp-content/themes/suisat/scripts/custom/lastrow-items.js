/**
 * Calculating last row items
 */

/* ============================================================= */
/*        Initializing Function/Methods on document load         */
/* ============================================================= */

// Init getLastRowCols
getLastRowCols(
    $('.js-row'),
    $('.js-last-col')
);

$(window).on('resize', function() {
    // Init getLastRowCols
    getLastRowCols(
        $('.js-row'),
        $('.js-last-col')
    );
});

//Function declaration - detecting last row elements
function getLastRowCols(container, elem) {
    container.each(function() {
        var $self = $(this);
        var colInRow = 0;
        //var $container = null;
        $self.find(elem).each(function() {
            if($(this).prev().length > 0) {
                if($(this).position().top != $(this).prev().position().top) return false;
                colInRow++;
            }
            else {
                colInRow++;
            }
            var colInLastRow = $self.find(elem).length % colInRow;
            if(colInLastRow == 0) colInLastRow = colInRow;
            $self.find(elem).removeClass('last-col')
            $self.find(elem).slice(-colInLastRow).addClass('last-col')
        });
    });
}