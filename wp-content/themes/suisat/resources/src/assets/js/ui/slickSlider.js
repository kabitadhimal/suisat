const slickSlider = () => {
	
	$('.slider .slider__slides').slick({
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: true,
		responsive: [
		   {
		     breakpoint: 768,
		     settings: {
		       slidesToShow: 1,
		       slidesToScroll: 1
		     }
		   }
	    ]
	});
};

export default slickSlider;