const googleMap = () => {
	$('.map').each(function() {
		var $map = $(this);
		var map_coords = new google.maps.LatLng( $map.data('lat'), $map.data('lng') );
		var mapId = $map.attr('id');
		var myOptions = {
			zoom: 15,
			center: map_coords,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
			
		var map = new google.maps.Map(document.getElementById(mapId), myOptions);
		
		var icon = {
		    url: $('#google-map').data('marker'),
		    size: new google.maps.Size(100, 144),
		    scaledSize: new google.maps.Size(50, 72)
		};
		
		var myMarker = new google.maps.Marker({
			position: map_coords,
			draggable: false,
			map: map,
			icon: icon
		});				
	});
}

export default googleMap;