<?php
/*
Template Name: News
*/

$inputCategory = '';
if(isset($_GET['cat'])) $inputCategory = (int) $_GET['cat'];
?>

<section class="section">

    <div class="section__bar">
        <div class="container container--no-padding">
            <h2>News</h2>
            <form method="get" class="form-filter"  id="block-frm" >
                <div class="form-row">
                    <div class="form__col--size1">
                        <div class="form-group">
                            <label for="select-1">Sort by</label>
                            <select id="select-1" class="select form-control js-block-frm-input" name="cat">

                                <option value="">ALL CATEGORIES</option>

                                <?php
                                $catTerms = get_categories(array(
                                    'hide_empty' => false,
                                ));
                                if(!empty($catTerms)):
                                    foreach($catTerms as $category):

                                        if(!empty($inputCategory) && ($category->term_id==$inputCategory)) :
                                            $selected = 'selected';
                                        else:
                                            $selected ='';
                                        endif;

                                        ?>
                                        <option value="<?=$category->term_id?>" <?=$selected?>><?=$category->name?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div><!-- /.form-group -->
                    </div>

                </div>
            </form>
        </div><!-- /.container -->
    </div><!-- /.section__bar -->

    <div class="section__body">
        <div class="container container--no-padding">
            <ul class="list-images news-block">
                <?php
                $actionUrl = get_permalink();
                //build query
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $args = [ 'post_type' => 'post', 'paged' => $paged,'order' => 'DESC','ignore_custom_sort' => TRUE,'orderby'=> 'post_date','post_status'=> 'publish'];

                $inputSizeCategories = '';
                if(isset($_GET['cat']) && $_GET['cat']!='undefined') {
                    $args['category__in'] = $_GET['cat'];
                }
                $query = new WP_Query( $args );
                if($query->have_posts()):
                    while($query->have_posts()): $query->the_post();
                        //get_template_part('partials/news-item');ship-list
                        ?>
                        <li>
                            <?php
                            // news-thumb
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
                            if($image) { $img = \App\getImageManager()->resize( \App\getImageDirectoryPath($image[0]), \App\IMAGE_SIZE_BLOCK_THUMB); }
                            ?>
                            <a  href="<?php the_permalink(); ?>" style="background-image: url(<?php echo $img; ?>)">
                                <img src="<?php echo $image[0]; ?>">
                                <h2>
                                    <span class="date"><?php  the_date('d/m/Y'); ?></span>
                                    <?php the_title(); ?>
                                </h2>
                            </a>
                        </li>
                    <?php
                    endwhile;
                ?>
            </ul><!-- /.list-images -->

            <?php
            \App\custom_pagination($query->max_num_pages,"",$paged, $actionUrl);
            else :
                echo 'Content does not match the filter criteria.';
            endif;
            ?>
        </div><!-- /.container -->
    </div><!-- /.section__body -->
</section><!-- /.section -->
<script>
    jQuery(document).ready(function () {
        var $blockForm = jQuery('#block-frm');
        jQuery('#block-frm .js-block-frm-input').change(function () {
            $blockForm.submit();
        });

    });

    jQuery(function($) {
        var paramCat = jQuery('#select-1 option:selected').val();
        jQuery('#select-1 ~ .select-styled').text(jQuery('li[rel="'+ paramCat +'"]').text());
    });

</script>

<script>
   /*
       jQuery( document ).ready(function() {
        var $blockForm = jQuery('#block-frm');
        jQuery('#select-1 ~ ul.select-options li').click(function() {
            var $news = jQuery(this).attr('rel');
            submitForm($news)
        });
        submitForm();
    });

   function submitForm($news) {

        // var frmData = 'echo $jaxTag ?>type=$contentType?>'+'&cat='+$catFilter +'&action=display_contents';
        var data = 'cat='+$news+'&action=news_contents';
        var ajaxURL = "admin_url('admin-ajax.php')?>";
        $.ajax({
            url: ajaxURL,
            data: data,
            success: function(response){
                if (response.length > 0) {
                    //    $(".not-found").hide();
                    $(".list-images").html(response);
                    // == Renit javascript codes related to these blocks
                }/!*else {
                 $(".not-found").show();
                 }*!/
            }
        });
    }
</script>
