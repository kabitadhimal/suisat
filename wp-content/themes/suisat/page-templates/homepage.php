<?php
/*
Template Name: Homepage
*/

include get_template_directory() . '/partials/home/main-block-1.php';
include get_template_directory() . '/partials/home/about-us-vision.php';
//include get_template_directory() . '/partials/home/vision-block-3.php';
include get_template_directory() . '/partials/home/services-block-4.php';
include get_template_directory() . '/partials/home/fleet-grid-5.php';
include get_template_directory() . '/partials/home/news-block-6.php';

