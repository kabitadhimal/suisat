var windowW = $(window).width();
var windowH = $(window).height();
var headerH = $('.navbar').outerHeight();

function menuHeightMobile(){
    if ($('.navbar-toggler').length){
        $('.navbar-toggler').on('click', function () {
            $('.nav-collapse').css({
                'height': windowH - headerH
            })
        });
    }
}

function navTogglerMobile() {


        $('.navbar-nav .dropdown a').click(function () {
            if (windowW < 768) {
            $(this).parents('.dropdown').siblings().removeClass('show');
            $(this).parents('.dropdown').toggleClass('show');
            $(this).parents('.dropdown').siblings().find('.dropdown-menu').slideUp();
            $(this).siblings('.dropdown-menu').slideToggle();
            }else{
                preventDefault();
            }
        })

}

var isClicked = false;
function menuAlignCenter() {

    var targetH = $('.navbar-nav').outerHeight();
    var heightDiff = parseInt((windowH - headerH - targetH), 10);
    var finalH = heightDiff / 2;

    /*$('.navbar-nav').css({
        marginTop: finalH
    });*/

    $('.nav-collapse').css({

        "top": headerH
    })

    if($("body").hasClass('page-template-homepage')){
        $('.nav-collapse').css({

            "top": headerH + 2
        })
    }
}

$( document ).ready(function() {
   // menuHeightMobile();
   // menuAlignCenter();

    $('.navbar-toggler').on('click', function () {
        $('body').toggleClass('overflow-hidden');
    })


        navTogglerMobile();





    $('.nav-link').on('click', function() {
    if ($(this).parent().hasClass('show')) {
        isClicked = true;
        $(this).parent().addClass('show');

    } else {
        isClicked = false;

        $(this).parent().removeClass('show');
    }
});


$('.navbar-toggler-icon').on('click', function () {
    $('.nav-collapse').toggleClass('show');
});

    $('.nav-collapse .close').on('click', function () {
        $('.nav-collapse').removeClass('show');
        $('.dropdown-menu').slideUp();
        $('.dropdown').removeClass('show');
    });


    $('#about-vision .carousel-inner').slick({
        infinite: true,
        asNavFor: '.carousel-indicators',
        prevArrow: $('.carousel-control-prev'),
        nextArrow: $('.carousel-control-next')
    });

    $('.carousel-indicators').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        asNavFor: '#about-vision .carousel-inner',
        dots: true,
        centerMode: true,
        focusOnSelect: true,
        infinite: false,
    });

});


$(window).on('resize orientationchange', function() {
    windowH = $(window).height();
    windowW = $(window).width();
    headerH = $('.navbar').outerHeight();


  //  if (isClicked === true) return;
   // menuAlignCenter();
   // menuHeightMobile();
});