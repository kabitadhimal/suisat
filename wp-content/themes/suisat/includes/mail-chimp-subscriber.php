﻿<?php
header("Content-Type: text/html;charset=utf-8");
extract($_POST);
$response = array();

if($email == ''){
    if(ICL_LANGUAGE_CODE=="fr") {
        $response['error'] = "Email est requis!";
    }else {
        $response['error'] = "Email is required!";
    }


}else if(!is_email($email)){
	//$response['error'] = 'Email is invalid!';

    if(ICL_LANGUAGE_CODE=="fr") {
        $response['error'] ="Email est invalide !";

    }else {
        $response['error'] ="Email is invalid!";
    }

}else{
	if(sendAddressToMailChimp($email, $lang) === true){
        if(ICL_LANGUAGE_CODE=="fr") {
            $response['success'] = "Vous êtes abonné. Merci." ;
        }else {
            $response['success'] = "You are subscribed. Thank you." ;
        }

		//$response['success'] = 'You are subscribed!. Thank you.';
	}else{
        if(ICL_LANGUAGE_CODE=="fr") {
            $response['success'] = "Vous étiez déjà abonné. Merci";
        }else {
            $response['success'] = "You were already subscribed. Thank you";
        }

	}
}
echo json_encode($response);
die();
