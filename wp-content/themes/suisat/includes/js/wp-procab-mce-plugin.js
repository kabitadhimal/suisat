(function() {
    tinymce.create('tinymce.plugins.wp_procab', {
        init : function(ed, url) {

            ed.addButton('btn-yellow',{
                title : 'Button Yellow',
                cmd	: 'btn-yellow',
                text : 'Button Yellow'
            });

            ed.addButton('btn-grey',{
                title : 'Button Grey',
                cmd	: 'btn-grey',
                text : 'Button Grey'
            });


            //Button Yellow
            ed.addCommand('btn-yellow', function() {
                ed.execCommand('mceInsertContent', 0, '[btn type="yellow" href="--yellow--" target="" ]-- add Label here  --[/btn]');
            });

            //button
            ed.addCommand('btn-grey', function() {
                ed.execCommand('mceInsertContent', 0, '[btn type="grey" href="--grey--" target=""]-- add Label here --[/btn]');
            });


        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : 'Wp Procab Buttons'
            };
        }
    });

    tinymce.PluginManager.add( 'wp_procab', tinymce.plugins.wp_procab );
})();