<?php
add_action('init', 'wp_procab_buttons');
function wp_procab_buttons(){
	add_filter('mce_external_plugins', 'add_wp_procab_buttons');
	add_filter( 'mce_buttons', 'wp_procab_register_buttons' );
}

function add_wp_procab_buttons($plugin_array){
	$plugin_array['wp_procab'] = get_template_directory_uri() . '/includes/js/wp-procab-mce-plugin.js';
	return $plugin_array;
}
function wp_procab_register_buttons( $buttons ) {
	array_push( $buttons,'btn-yellow','btn-grey');
	return $buttons;
}
