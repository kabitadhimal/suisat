<?php
namespace App\Menu;

class Primary extends \Walker_Nav_Menu{
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);

        $output .= "<ul class='submenu'>\n";
       /* switch($depth){
            case 1:
                $output .= '<ul class="menu-category__list" role="menu">';
                break;
            case 2:  $output .= "<div class=\"menu__expandable-wrap js-category-slug\"><ul class='menu__expandable'>";
                break;
            default:
                $output .= "<ul class='submenu'>\n".$depth;
                break;
        }*/
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );

    /*    if($depth == 0){
            //$output .= '</div>';
            $output .= '</ul>';
        } elseif ($depth==2) {
            $output .= '</ul>';
        }else{
            $output .= "$indent</ul>{$n}";
        }*/

        $output .= '</ul>';

    }

    private function getElementAttributes($item, $depth, $args){
        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';
        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }
        return $attributes;
    }

    private function getElementCssClass($item, $depth, $args){
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        //$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

     //   $class_names = str_replace('menu-item-has-children', 'has-submenu', esc_attr($class_names));

        return $class_names;
    }



    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

       // $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );
        //$item_output = $args->before;
        //$item->title = $item->title . ' => '. $depth;
        $item_output = '';

        $class_names = $this->getElementCssClass($item, $depth, $args);
        $attributes = $this->getElementAttributes($item, $depth, $args);


        $arrayClass = explode(' ',$class_names);

                if (in_array("menu-item-has-children", $arrayClass)) {
                    $item_output .='<li class="menu-item menu-item-has-children"><a href="#">'.$item->title.'</a><a href="#" class="toggle-submenu"></a>';
                } else {

                    $item_output .= '<li class="menu-item submenu-item">
                                                    <a '.$attributes.' ">'.$item->title.'</a>';

                }


        $output .= $item_output;


    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }




                $output .= '</li>';





        //$output .= parent::end_el($output, $item, $depth, $args);

    }


    public function start_elx( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        $title =  $item->title;

        $item_output = $args->before;

        $item_output .= $title;


        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

}