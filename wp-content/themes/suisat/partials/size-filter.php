<?php

//build query
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = [ 'post_type' => 'fleet', 'paged' => $paged, 'paged' => $paged,'order' =>'ASC', 'orderby'=>'title','posts_per_page' => -1,'post_status'=> 'publish'];

$inputSizeCategories = '';
if(isset($_GET['size'])) $inputSizeCategories = (int) $_GET['size'];
if($inputSizeCategories){
    $taxQuery = [
        'taxonomy' => 'size',
        'field'    => 'term_id',
        'terms'    => $inputSizeCategories,
    ];

    $args['tax_query'] = [$taxQuery];
}
$query = new WP_Query( $args );


if($query->have_posts()):
    while($query->have_posts()): $query->the_post();
        //get_template_part('partials/news-item');ship-list
        include(locate_template("partials/block/ship-list.php"));
    endwhile;
else :
    echo 'Le contenu ne correspond pas aux critères de filtrage';
endif;
?>