<?php
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>
<section class="section section--simple <?php echo $section_gap_type; ?>">
    <div class="container container--no-padding">
        <div class="accordion" id="accordionExample">
            <?php
            $contents = $block['contents'];
            $backGroundColor ="bg-".$bgColor;
            $count=1;

            foreach ($contents as $content):
            $title = $content['title'];
            $text = $content['text'];
            $ariaExpand = "false";
            $collapse = "btn-link collapsed";
            $show ="";

            if($count==1):
                $ariaExpand = "true";
                $collapse = "";
                $show ="show";
            endif;
            ?>

            <div class="accordion__section">
                <div id="heading<?=$count?>">
                    <h4 class="mb-0">
                        <button class="<?=$collapse?>" type="button" data-toggle="collapse" data-target="#collapse<?=$count?>" aria-expanded="<?=$ariaExpand?>" aria-controls="collapse<?=$count?>">
                        <?=$title?>
                        </button>
                    </h4>
                </div>
                <div id="collapse<?=$count?>" class="collapse <?=$show?>" aria-labelledby="heading<?=$count?>" data-parent="#accordionExample">
                    <div class="accordion__body">
                        <?=$text?>
                        <?php if(!empty($content['contains_table'])):
                            $table = $content['table'];
                            if(!empty($table)):
                            ?>
                                <div class="table__wrapper">
                                    <table class="table table-simple">
                                        <?php     foreach ($table['body'] as $tr) {
                                            echo '<tr>';
                                            $counter = 0;
                                            foreach ($tr as $td) {
                                                echo '<td>';
                                                echo $td['c'];
                                                echo '</td>';
                                                $counter++;
                                            }
                                            echo '</tr>';
                                        }
                                        ?>
                                    </table>
                                </div><!-- /.table__wrapper -->
                                <?php endif; ?>
                        <?php endif; ?>
                    </div><!-- /.accordion__body -->
                </div>
            </div><!-- /.accorcion__section -->
            <?php $count++;
                endforeach;
            ?>
        </div>
    </div><!-- /.container -->
</section><!-- /.section -->
