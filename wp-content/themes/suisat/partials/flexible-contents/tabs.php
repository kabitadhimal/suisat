<?php
$contents = $block['contents'];
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>
<section class="section section--simple <?php echo $section_gap_type; ?>">
    <div class="container container--no-padding">
        <div class="tabs">
            <ul class="nav nav-tabs">
                <?php
                $count =1;
                foreach ($contents as $content):

                        if(!empty($content['activate_tab'])) :
                            $activeTab = "active";
                        else :
                            $activeTab = "";
                        endif;
                    ?>
                <li class="nav-item">
                    <a data-toggle="tab" href="#menu<?=$count?>" class="nav-link <?=$activeTab?>"><?=$content['title']?></a>
                </li>
                <?php $count++; endforeach; ?>
            </ul>

            <div class="tab-content" id="content">
                <?php
                $count =1;
                foreach ($contents as $content):
                    $activeTab = "";
                    $show ="";
                    if(!empty($content['activate_tab'])) :
                        $activeTab = " show active";
                        $show ="show";
                    endif;
                ?>
                <div id="menu<?=$count?>" class="tab-pane fade <?=$activeTab?>">
                    <a data-toggle="collapse" href="#collapse-<?=$count?>" data-parent="#content" aria-expanded="true" aria-controls="collapse-<?=$count?>">
                        <?=$content['title']?>
                    </a>
                    <div class="collapse <?=$show?>" id="collapse-<?=$count?>" role="tabpanel" aria-labelledby="heading-<?=$count?>">
                        <div class="row">
                            <?php if($content['col_1']): ?>
                                <div class="col-sm-6">
                                    <?=$content['col_1']?>
                                </div><!-- /.col-sm-6 -->
                            <?php endif; ?>
                            <?php if($content['col_2']): ?>
                                <div class="col-sm-6">
                                    <?=$content['col_2']?>
                                </div><!-- /.col-sm-6 -->
                            <?php endif; ?>
                        </div><!-- /.row -->
                    </div>
                </div>
                <?php $count++; endforeach; ?>
            </div>
        </div>
    </div>
</section>