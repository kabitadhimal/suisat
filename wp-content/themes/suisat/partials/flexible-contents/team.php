<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 6/5/2018
 * Time: 12:59 PM
 */
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
if(isset($_GET['boat'])) $inputSizeCategories = (int) $_GET['boat'];
$inputSizeCategories = '';
if(isset($_GET['size'])) $inputSizeCategories = (int) $_GET['size'];
if(isset($_GET['type'])) $inputTypeCategories = (int) $_GET['type'];

?>
<section class="section">
    <div class="section__bar <?php echo $section_gap_type; ?>">
        <div class="container container--no-padding">
            <h2>Fleet</h2>
            <form name="filter-form" class="form-filter" id="js-form">
                <div class="form-row">
                    <div class="form__col--size1">
                        <div class="form-group">
                            <label for="select-1">Sort by</label>
                            <select name="size" id="select-1" class="form-control js-select">
                                <option value="">Size</option>
                                <?php

                                $sizeTerms = get_terms( 'size', array(
                                    'hide_empty' => false,
                                ) );
                                if(!empty($sizeTerms)):
                                    foreach($sizeTerms as $category):
                                        if(!empty($inputSizeCategories) && ($category->term_id==$inputSizeCategories)) :
                                            $selected = 'selected';
                                        else:
                                            $selected ='';
                                        endif;
                                        ?>
                                        <option value="<?=$category->term_id?>" <?=$selected?> ><?=$category->name?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>

                            </select>
                        </div><!-- /.form-group -->
                    </div>
                    <div class="form__col--size2">
                        <div class="form-group">
                            <label for="select-3">Sort by</label>
                            <select id="select-3" class="form-control js-select" name="type">
                                <option value="">Type</option>
                                <?php
                                $typeTerms = get_terms( 'type', array(
                                    'hide_empty' => false,
                                ) );
                                if(!empty($typeTerms)):
                                    foreach($typeTerms as $category):
                                        if(!empty($inputTypeCategories) && ($category->term_id==$inputTypeCategories)):
                                            $selected = 'selected';
                                        else:
                                            $selected ='';
                                        endif;
                                        ?>
                                        <option value="<?=$category->term_id?>" <?=$selected?> ><?=$category->name?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div><!-- /.form-group -->
                    </div>
                </div>
            </form>
        </div><!-- /.container -->
    </div><!-- /.section__bar -->
    <div class="section__body">
        <div class="container container--no-padding">
            <ul class="list-images">
            </ul>
            <div class="not-found" style="color: #ff0000"> <?php echo "Content doesn't match the filter criteria "; ?></div>
        </div><!-- /.container -->
    </div><!-- /.section__body -->
</section><!-- /.section -->
<script>
        $( "select.js-select" ).on( "change", function( event ) {
            event.preventDefault();
            var $dynFilter = $( "#js-form" ).serialize();
            submit($dynFilter);
        });
        function submit($dynFilter) {
            var frmData = $dynFilter+'&action=fleet_contents';
            $.ajax({
                url: "<?=admin_url('admin-ajax.php')?>",
                data:frmData,
                success: function(response){
                    if (response.length > 0) {
                        $(".not-found").hide();
                        $(".list-images").html(response);
                    }else {
                        $(".not-found").show();
                    }
                }
            });
        }
        submit();
</script>