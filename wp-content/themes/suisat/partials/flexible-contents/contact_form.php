<?php
$bgColor = $block['background_color'];
$backGroundColor ="bg-".$bgColor;
$formText = $block['form_text'];
$contactForm = $block['contact_shortcode'];
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection($section_gap);
if(!empty($contactForm)):
?>
    <section class="section section--simple <?=$section_gap_type?> <?=$backGroundColor?>">
        <div class="container">

            <div class="form-default">
                    <?php if(!empty($formText)): ?>
                    <div class="form__head">
                     <?=$formText?>
                    </div><!-- /.form__head -->
                <?php endif; ?>
                <?php echo do_shortcode($contactForm);?>
            </div>
        </div><!-- /.container -->
    </section>
<?php endif;