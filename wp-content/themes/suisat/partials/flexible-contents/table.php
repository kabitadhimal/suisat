<?php
$bgColor = $block['background_color'];
$backGroundColor ="bg-".$bgColor;
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );

?>
<section class="section section--simple <?=$backGroundColor?> <?php echo $section_gap_type; ?>">
    <div class="container container--no-padding">
        <div class="table__wrapper">
        <?php
        $table = $block['table'];
        if(!empty($table)) :
                $th_group = [];
                if ($table) {
                    echo '<table class="table table-striped table--secondary">';
                    echo '<thead>';
                    if ($table['header']) {
                        echo '<tr>';
                        foreach ($table['header'] as $th) {
                            echo '<th>';
                            echo $th['c'];
                            echo '</th>';
                            array_push($th_group, $th['c']);
                        }
                        echo '</tr>';
                        echo '</thead>';
                    }
                    echo ' <tbody>';
                    foreach ($table['body'] as $tr) {
                        echo '<tr>';
                        $counter = 0;
                        foreach ($tr as $td) {
                            echo '<td data-th-title="' . $th_group[$counter] . '">';
                            echo $td['c'];
                            echo '</td>';
                            $counter++;
                        }
                        echo '</tr>';
                    }
                    echo '</tbody>';
                    echo '</table>';
                }
            ?>
        <?php endif; ?>
        </div>
    </div>
</section>