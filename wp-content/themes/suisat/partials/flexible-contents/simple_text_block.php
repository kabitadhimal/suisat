<?php
$title = $block['title'];
$subtitle = $block['subtitle'];
$bgColor = $block['background_color'];
$backGroundColor ="bg-".$bgColor;
$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );
?>

<section class="section <?=$backGroundColor?>">
    <div class="container">
        <div class="section__group text-center <?php echo $section_gap_type; ?>">
            <?php if(!empty($title)): ?>
            <h2><?=$title?></h2>
            <?php endif; ?>
            <?php if(!empty($subtitle)): ?>
                <?php echo $subtitle; ?>
            <?php endif; ?>
        </div><!-- /.section__group -->
    </div><!-- /.container -->
</section><!-- /.section -->