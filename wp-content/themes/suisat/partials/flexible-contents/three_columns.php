<?php
$cols = $block['columns'];

$section_gap = $block['section_gap'];
$section_gap_type = \App\_ps_gap_selection( $section_gap );

if(!empty($cols)):
?>
<section class="section section--simple <?php echo $section_gap_type; ?>">
    <div class="container container--no-padding">
        <div class="card-group">

            <?php foreach ($cols as $colContent) :
                    $image = $colContent['image'];
                    $title = $colContent['title'];
                    $text = $colContent['text'];
                    $link = $colContent['link'];
                    $bgColor = $colContent['background_color'];
                    $backGroundColor ="bg-".$bgColor;
                ?>
                <div class="card <?=$backGroundColor?>">
                    <?php if(!empty($image)): ?>
                    <div class="card__image card__image--large">
                        <img src="<?php echo $image; ?>" alt="image">
                    </div><!-- /.card__image -->
                    <?php endif; ?>
                    <div class="card-body">
                        <?php if(!empty($title)): ?>
                            <h4 class="card-title"><?=$title?></h4>
                        <?php endif; ?>

                        <?php if(!empty($text)): ?>
                        <?php echo '<p class="card-text">'.$text.'</p>' ;
                            if(!empty($link)):
                                if(!empty($link['target'])) :
                                    $target='target="_blank"';
                                endif;
                            ?>
                                <a href="<?=$link['url']?>" <?=$target?> class="btn btn--yellow"><?=$link['title']?></a>
                        <?php
                            endif;
                        endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        </div>
    </section>
<?php endif;