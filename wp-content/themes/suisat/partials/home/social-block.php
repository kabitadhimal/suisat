<section class="block block--text gap-p-eq is-extended-full wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">

    <header class="block__h text-center">
        <h2 class="mb-0 mb-sm-2 vertical-line text-uppercase">suivez-nous</h2>
        <p class="sub-title mb-0 mb-sm-4">Partagez l’expérience</p>
    </header>

    <ul class="d-flex flex-wrap insta-list align-items-center bg-primary">
        <li><figure class="mb-sm-0 mb-0"><img alt="Post Thumbnail" class="img img-full img-fluid" src="<?php echo get_template_directory_uri(); ?>/contents/insta1.jpg"></figure></li>
        <li><figure class="mb-sm-0 mb-0"><img alt="Post Thumbnail" class="img img-full img-fluid" src="<?php echo get_template_directory_uri(); ?>/contents/insta2.jpg"></figure></li>
        <li class="text-white text-center">
            <span><a href=""><i class="icon icon-instagram"></i></a></span>
            <span class="text-white">#spavalmontkempinski</span>
            <span class="text-white">@spavalmontkempinski</span>
            <span><a href=""><i class="icon icon-facebook"></i></a></span>
        </li>
        <li><figure class="mb-sm-0 mb-0"><img alt="Post Thumbnail" class="img img-full img-fluid" src="<?php echo get_template_directory_uri(); ?>/contents/insta4.jpg"></figure></li>
        <li><figure class="mb-sm-0 mb-0"><img alt="Post Thumbnail" class="img img-full img-fluid" src="<?php echo get_template_directory_uri(); ?>/contents/insta2.jpg"></figure></li>
    </ul>
</section>
