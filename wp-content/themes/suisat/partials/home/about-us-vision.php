<?php
$aboutLeft = get_field('about_left_section');
$leftTitle = $aboutLeft['big_title'];
$leftText = $aboutLeft['text'];

$aboutRight = get_field('about_right_section');
$sectonName = $aboutRight['section_name'];
$rightTitle = $aboutRight['title'];
$link = $aboutRight['right_link'];
$image = $aboutRight['image'];
?>
<section class="section">
    <div class="cols">
        <div class="col col--size1">
            <div class="section__inner">
                <?php if(!empty($leftTitle)): ?> <h2><?=$leftTitle?></h2><?php endif; ?>
                <?php if(!empty($leftText)): ?><?=$leftText?><?php endif; ?>
            </div><!-- /.section__inner -->
        </div><!-- /.col col-/-size1 -->

        <div class="col col--size2" style="background-image: url(<?php echo $image; ?>)">
            <div class="section__content">
                <?php if(!empty($sectonName)): ?><h5><?=$sectonName?></h5><?php endif; ?>
                <?php if(!empty($rightTitle)): ?><h3><?=$rightTitle?></h3><?php endif; ?>
                <?php if(!empty($link)):
                    if(!empty($link['target'])) :
                    $target='target="_blank"';
                    endif;
                    ?>
                    <a href="<?=$link['url']?>" <?=$target?> class="link-more"><?=$link['title']?></a>
                <?php
                endif;?>
            </div><!-- /.section__content -->
        </div><!-- /.col col-/-size2 -->
    </div><!-- /.cols -->

    <?php
    $visionSlider = get_field('vision_slider');
    $link =  get_field('link');
    $i=0;
   $countImg = count($visionSlider);
    ?>

    <div id="about-vision" class="carousel slide" data-ride="carousel">
        <div class="actions">
            <div class="carousel-indicators-wrap">
        <div class="carousel-indicators">
            <?php
            while( $i < $countImg) {
            $activeClass = ($i == 0 ? 'class="active"' : ''); ?>
                <div data-slide-to="<?=$i?>" <?=$activeClass?>></div>
            <?php $i++;} ?>
        </div>

                <a class="carousel-control-prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        <?php
        if(!empty($link)):
            if(!empty($link['target'])) :
                $target='target="_blank"';
            endif;
            ?>
            <a href="<?=$link['url']?>" <?=$target?> class="link-more"><?=$link['title']?></a>
        <?php
        endif;?>
        </div>

        <div class="carousel-inner">
            <?php
            $i=0;

            if(!empty($visionSlider)):
            foreach ($visionSlider as $slider) {
                $title = $slider['title'];
                $text = $slider['text'];
                $photo = $slider['image'];
                $icon = $slider['icon'];
                $activeClass = ($i == 0 ? 'active' : '');
                if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_VISION_SLIDER);
            ?>
            <div class="carousel-item <?=$activeClass?>">
                <?php if(!empty($photo)): ?>
                    <img class="d-block w-100" src="<?php echo $photo; ?>" alt="First slide">
                <?php endif; ?>
                <div class="carousel-caption d-none d-md-block">
                    <h2>
                        <img class="icon" src="<?php echo $icon; ?>">
                        <?php if(!empty($title)) { echo $title;} ?>
                    </h2>
                    <?php if(!empty($text)): echo $text; endif; ?>
                </div>
            </div>
            <?php $i++; } endif; ?>
        </div>

    </div>
</section><!-- /.section -->