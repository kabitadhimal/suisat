<?php
//build query
$args = [ 'post_type' => 'post','posts_per_page' => -1,'order' => 'DESC','ignore_custom_sort' => TRUE,'orderby'=> 'post_date','post_status'=> 'publish'];
$inputSizeCategories = '';
if(isset($_GET['cat']) && $_GET['cat']!='undefined') {
    $args['category__in'] = $_GET['cat'];
}
$query = new WP_Query( $args );
if($query->have_posts()):
    while($query->have_posts()): $query->the_post();
        //get_template_part('partials/news-item');ship-list
        ?>
        <li>
            <?php
            // news-thumb
            $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
            if($image) { $img = \App\getImageManager()->resize( \App\getImageDirectoryPath($image[0]), \App\IMAGE_SIZE_BLOCK_THUMB); }
            ?>
            <a  href="<?php the_permalink(); ?>" style="background-image: url(<?php echo $img; ?>)">
                <img src="<?php echo $image[0]; ?>">
                <h2>
                <span class="date"><?php  the_date('d/m/Y'); ?></span>
                <?php the_title(); ?>
                </h2>
            </a>
        </li>
        <?php
    endwhile;
else :
    echo 'Content does not match the filter criteria.';
endif;
