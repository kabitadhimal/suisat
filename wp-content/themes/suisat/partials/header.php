<nav class="navbar navbar-expand-lg fixed-top">
    <a class="navbar-brand" href="<?php echo home_url(); ?>">
        Suisse - Atlantique
        <?php  $headerLogo = get_field('header_logo','options');
            $headerLogo = !empty($headerLogo)?$headerLogo:get_template_directory_uri().'/assets/images/temp/logo@2x.png';
            if(!empty($headerLogo)):
        ?>
             <img src="<?php echo $headerLogo;?>" alt="logo" style="width: 15.5rem; height: 3.3rem;">
        <?php endif; ?>
    </a>
    <button class="navbar-toggler-icon">
        <span class="line-1"></span>
        <span class="line-2"></span>
        <span class="line-3"></span>
    </button>
    <!--<div class="collapse navbar-collapse" id="navbarSupportedContent">-->
    <div class="nav-collapse">
        <ul class="navbar-nav ml-auto">

            <?php
            if ( has_nav_menu( 'primary_menu' ) ) :
                wp_nav_menu( array(
                    'theme_location' => 'primary_menu',
                    'items_wrap' => '%3$s',
                    'container' => '',
                    'container_class' => '',
                    'container_id' =>'',
                    'walker'  => new Walker_Quickstart_Menu()
                ) );
            endif;
            ?>
                <li class="nav-item">
                        <?php  if(is_user_logged_in()) : ?>
                    <a class="nav-link disabled" href="<?php echo admin_url(); ?>">
                        <i class="ico-profile-white"></i>
                        <i class="ico-profile"></i>
                    </a>
                        <?php endif; ?>
                </li>

        </ul>

        <a href="javascript:void()" class="close">
            <img src="<?php echo get_template_directory_uri()?>/assets/images/close.svg" alt="">
        </a>
    </div>
</nav>
