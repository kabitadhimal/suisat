<!doctype html>
<html <?php language_attributes(); ?>>
<?php get_template_part('partials/head'); ?>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NLVKHK" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
    $class="wrapper--interior";
     if(is_front_page() || is_home()) {
         $class = "";
     }
 ?>
<div class="wrapper <?=$class?>">
    <?php
    do_action('get_header');
    get_template_part('partials/header');
    ?>
    <div class="main">
                <?php include \App\get_main_template(); ?>
    </div>
        <?php
        do_action('get_footer');
        get_template_part('partials/footer');
        ?>
</div>
<?php wp_footer(); ?>
</body>
</html>